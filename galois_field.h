#pragma once

#include "util.h"
#include "irreducable_of.h"
#include <concepts>
#include <type_traits>
#include <limits>
#include <compare>

template <class BaseType, class PolynomialType = irreducable_of<BaseType>>
class galois_element
{
	static_assert((PolynomialType::degree > 0) && (PolynomialType::degree <= sizeof(BaseType) * CHAR_BIT), "Invalid Galois field polynomial");
public:
	using value_type = BaseType;
private:
	using signed_base_type = std::make_signed_t<value_type>;
	using unsigned_base_type = std::make_unsigned_t<value_type>;
	value_type m_val = value_type();
	constexpr static poly_div_t<galois_element> my_poly_div(const galois_element& left, const galois_element& right)
	{
		auto qr = poly_div(left.m_val, right.m_val);
		return poly_div_t<galois_element>{qr.quot, qr.rem};
	}
	constexpr static poly_div_t<galois_element> my_poly_div(const galois_element& left, PolynomialType)
	{
		auto qr = poly_div(static_cast<typename PolynomialType::base_type>(left.m_val), PolynomialType());
		return poly_div_t<galois_element>{static_cast<value_type>(qr.quot), static_cast<value_type>(qr.rem)};
	}
	constexpr static poly_div_t<galois_element> my_poly_div(PolynomialType left, const galois_element& right)
	{
		auto qr = poly_div(PolynomialType(), static_cast<typename PolynomialType::base_type>(right.m_val));
		return poly_div_t<galois_element>{static_cast<value_type>(qr.quot), static_cast<value_type>(qr.rem)};
	}
public:
	using polynomial = PolynomialType;
	static constexpr polynomial irreducable_polynomial = polynomial();
	galois_element() = default;
	constexpr galois_element(value_type val) noexcept:m_val(value_type(polynomial::adjust_value(val))) {}
	constexpr galois_element(polynomial) noexcept {}
	template <class TargetBaseType, class = std::enable_if_t<(sizeof(TargetBaseType) >= sizeof(value_type))>>
	constexpr operator galois_element<TargetBaseType, polynomial>() const noexcept
	{
		return galois_element<TargetBaseType, polynomial>(m_val);
	}
	constexpr operator value_type() const noexcept
	{
		return m_val;
	}
	template <class IntegralType, class = std::enable_if_t<std::is_integral_v<IntegralType>>>
	constexpr explicit operator IntegralType() const noexcept
	{
		return IntegralType(m_val);
	}
	constexpr galois_element operator+(const galois_element& right) const noexcept
	{
		return m_val ^ right.m_val;
	}
	constexpr galois_element operator-() const noexcept
	{
		return *this;
	}
	constexpr galois_element operator-(const galois_element& right) const noexcept
	{
		return m_val ^ right.m_val;
	}
	constexpr galois_element operator*(const galois_element& right) const noexcept
	{
		value_type result = 0;
		std::size_t i = 0;
		auto [a, b] = m_val < right.m_val?std::tuple{unsigned_base_type(m_val), unsigned_base_type(right.m_val)}
			:std::tuple{unsigned_base_type(right.m_val), unsigned_base_type(m_val)};
		auto shift = value_type(b);
		while (a != 0)
		{
			if ((a & 1) != 0)
				result ^= shift;
			shift = BaseType(polynomial::left_shift(shift));
			a >>= 1;
		}
		return result;
	}
	constexpr galois_element reciprocal() const
	{
		return galois_element(qr_inverse(*this, irreducable_polynomial, [](const auto& left, const auto& right) {return my_poly_div(left, right);}));
	}
	constexpr galois_element operator/(const galois_element& right) const
	{
		return *this * right.reciprocal();
	}	
	galois_element& operator+=(const galois_element& right) noexcept
	{
		return *this = *this + right;
	}
	galois_element& operator-=(const galois_element& right) noexcept
	{
		return *this = *this - right;
	}
	galois_element& operator*=(const galois_element& right) noexcept
	{
		return *this = *this * right;
	}
	galois_element& operator/=(const galois_element& right) noexcept
	{
		return *this = *this / right;
	}
};

template <std::unsigned_integral T1, std::unsigned_integral T2, class P>
constexpr auto operator<=>(const galois_element<T1, P>& left, const galois_element<T2, P>& right) noexcept
{
	return static_cast<T1>(left) <=> static_cast<T2>(right);
}

template <std::unsigned_integral T, class P, std::convertible_to<galois_element<T, P>> U>
constexpr auto operator<=>(const galois_element<T, P>& left, U right) noexcept
{
	return left <=> galois_element<T, P>(right);
}

template <std::unsigned_integral T, class P, std::convertible_to<galois_element<T, P>> U>
constexpr auto operator<=>(U left, const galois_element<T, P>& right) noexcept
{
	return galois_element<T, P>(left) <=> right;
}

constexpr galois_element<unsigned> operator""_ge(unsigned long long val) noexcept
{
	return unsigned(val);
}

template <class UnderlyingType, class PolynomialType, std::integral Power>
constexpr galois_element<UnderlyingType, PolynomialType> pow(galois_element<UnderlyingType, PolynomialType> x, Power pw) noexcept
{
	if (!pw)
		return galois_element<UnderlyingType, PolynomialType>{UnderlyingType(1)};
	else if (pw < Power{})
		return pw == std::numeric_limits<Power>::min()?(pow(x, -(std::numeric_limits<Power>::min() + 1)) * x).reciprocal():pow(x.reciprocal(), -pw);
	else if (x == galois_element<UnderlyingType, PolynomialType>{})
		return galois_element<UnderlyingType, PolynomialType>{};
	else
	{
		galois_element<UnderlyingType, PolynomialType> result{UnderlyingType(1)};
		if constexpr (PolynomialType::degree < std::numeric_limits<Power>::digits - 1)
			pw %= (Power{1} << PolynomialType::degree) - 1;
		galois_element<UnderlyingType, PolynomialType> g{x};
		while (pw > Power{})
		{
			if (pw & Power{1})
				result *= g;
			g *= g;
			pw >>= 1;
		}
		return result;
	}
}

template <class UnderlyingType, class PolynomialType>
constexpr bool is_perfect_square(galois_element<UnderlyingType, PolynomialType> x) noexcept
{
	return true;
}

template <class UnderlyingType, class PolynomialType>
constexpr galois_element<UnderlyingType, PolynomialType> sqrt(galois_element<UnderlyingType, PolynomialType> x) noexcept
{
	//For all x in GF(n): pow(x, n) == x <=> sqrt(x) = pow(x, n / 2).
	return pow(x, 1u << (PolynomialType::degree - 1));
}


