#include <type_traits>
#include <stdexcept>
#include <limits>
#include "bit_scan.h"

#ifndef ABS_ALG_GCD_G_
#define ABS_ALG_GCD_G_

namespace impl {
template <class T> requires (!std::is_signed_v<T>)
constexpr T abs(T x) noexcept {return x;}
template <std::signed_integral T>
constexpr T abs(T x) noexcept {return x < T{}?-x:x;}
}//impl

template <class T1, class T2, class...Ts>
constexpr std::common_type_t<T1, T2, Ts...> gcd(T1 x1, T2 x2, Ts...rest) noexcept
{
	if constexpr (sizeof...(Ts) == 0)
		return x2 == T2()?impl::abs(x1):gcd(impl::abs(x2), impl::abs(x1 % x2));
	else
		return gcd(gcd(x1, x2), rest...);
}

template <class T1, class T2, class ... Ts>
constexpr auto lcm(T1 x1, T2 x2, Ts...rest)
{
	if constexpr (sizeof...(Ts) > 0)
		return lcm(lcm(x1, x2), rest...);
	else
	{
		auto g = gcd(x1 = impl::abs(x1), x2 = impl::abs(x2));
		if (!g)
			return g;
		using CT = decltype(g);
		x1 /= g;
		if constexpr (std::is_integral_v<T1> && std::is_integral_v<T2>)
		{
			if ((bsr(x1) + 1) % (std::numeric_limits<T1>::digits + 1) + (bsr(x2) + 1) % (std::numeric_limits<T2>::digits + 1) > std::numeric_limits<CT>::digits + 1)
				throw std::overflow_error("Computation of LCM results in integral overflow");
		}
		return static_cast<CT>(x1) * static_cast<CT>(x2);
	}
}

struct non_invertible_value:std::invalid_argument
{
	inline non_invertible_value():std::invalid_argument("A value of a ring is not invertible") {}
};

template <class Oper, class T, class U>
inline auto qr_inverse_euclid_result_type_fn(Oper op, T x, U y)
{
	auto [q, r] = op(x, y);
	return r;
}

template <class T>
constexpr auto qr_inverse_euclid_is_null(T val) noexcept -> decltype(std::declval<T>() == T(0))
{
	return val == T(0);
}

constexpr bool qr_inverse_euclid_is_null(...) noexcept
{
	return false;
}

template <class T>
constexpr auto qr_inverse_euclid_is_one(T val) noexcept -> decltype(std::declval<T>() == T(1))
{
	return val == T(1);
}

constexpr bool qr_inverse_euclid_is_one(...) noexcept
{
	return false;
}

template <class T, class U, class Subtraction, class Division, class Q = decltype(qr_inverse_euclid_result_type_fn(std::declval<Division>(), std::declval<T>(), std::declval<U>()))>
constexpr auto qr_inverse_euclid(Subtraction sub, Division div, T r1, U r2, Q s1 = 1, Q s2 = 0)
{
	if (qr_inverse_euclid_is_null(r2))
		throw non_invertible_value();
	if (qr_inverse_euclid_is_one(r2))
		return s2;
	auto [q, r] = div(r1, r2);
	auto s = sub(Q(s1), Q(q * s2));
	return qr_inverse_euclid(sub, div, r2, r, s2, s);
}

template <class T>
struct euclidean_division_result
{
	T quot, rem;
};

struct euclidean_division
{
	template <class T>
	constexpr euclidean_division_result<T> operator()(T x, T y) const
	{
		if (!y)
			throw std::domain_error("Zero divisor");
		return euclidean_division_result<T>{static_cast<T>(x / y), static_cast<T>(x % y)};
	}
};

struct subtraction
{
	template <class T>
	constexpr T operator()(T x, T y) const noexcept
	{
		return x - y;
	}
};

template <class T>
struct subtraction_modulo
{
	T mod;
	constexpr T operator()(T x, T y) const noexcept
	{
		x %= mod;
		y %= mod;
		if (x >= y)
			return x - y;
		y = mod - y;
		return (x + y) % mod;
	}
};

template <class T, class U, class Division = euclidean_division>
constexpr auto qr_inverse(T value, U quotient, Division div = Division())
{
	if constexpr (std::is_integral_v<T> && std::is_integral_v<U>)
		return qr_inverse_euclid(subtraction_modulo{quotient}, div, value % quotient, quotient);
	else
		return qr_inverse_euclid(subtraction{}, div, value, quotient);
}

#endif //ABS_ALG_GCD_G_
