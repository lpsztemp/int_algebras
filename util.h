#include <climits>
#include "is_prime.h"
#include "gcd.h"
#include <ranges>

#pragma once

template <class T>
constexpr static T add_mod(T a, T b, T mod) noexcept
{
	if (mod == T())
		return a + b;
	else
	{
		T result = a + b; //result < 4*w
		if (result < a)
			return (result % mod + -mod % mod) % mod;
		return result % mod;
	}
}

template <class T>
constexpr static T mul_mod(T a, T b, T mod) noexcept
{
	if (!mod)
		return a * b;
	else
	{
		T x1 = a >> sizeof(T) * CHAR_BIT / 2; //high half
		T x0 = a & (T) -1 >> sizeof(T) * CHAR_BIT / 2; //low half
		T y1 = b >> sizeof(T) * CHAR_BIT / 2; //high half
		T y0 = b & (T) -1 >> sizeof(T) * CHAR_BIT / 2; //low half
		T res_lo = x0 * y0;
		T res_hi = x1 * y1;
		T interm1 = x0 * y1;
		T interm2 = x1 * y0;
		T tmp = interm1 + interm2;
		if (interm1 >= static_cast<T>(-interm2) && interm2 != T())
			interm1 = (tmp >> sizeof(T) * CHAR_BIT / 2) + (static_cast<T>(1) << sizeof(T) * CHAR_BIT / 2);
		else
			interm1 = tmp >> sizeof(T) * CHAR_BIT / 2;
		interm2 = tmp << sizeof(T) * CHAR_BIT / 2;
		res_hi += interm1;
		if (interm2 >= T(-res_lo) && res_lo != 0)
			++res_hi;
		res_lo += interm2;
		if (res_hi)
			return add_mod(static_cast<T>(res_lo % mod), mul_mod(static_cast<T>(res_hi % mod), static_cast<T>(static_cast<T>(-mod) % mod), mod), mod);
		return  res_lo % mod;
	}
}

/*
If modulo == 0, the function returns power p modulo (1 << std::numeric_limits<M>::digits).
Note that if std::numeric_limits<M>::digits >= std::numeric_limits<T>::digits, then
p mod (1 << std::numeric_limits<M>::digits) = p mod std::numeric_limits<T>::digits. 
*/
template <std::integral T, std::integral P, std::integral M>
constexpr T pow(T value, P power, M modulo)
{
	using U = std::make_unsigned_t<std::common_type_t<T, M>>;
	if constexpr (std::is_signed_v<M>)
		if (modulo < M{})
			modulo = -modulo;
	if (modulo == M{1})
		return T{};
	auto m = static_cast<U>(modulo);
	U v;
	if (value >= T{})
		v = m?static_cast<U>(value) % m:static_cast<U>(value);
	else if constexpr (std::is_signed_v<T>)
		v = m?m - static_cast<U>(-value) % m:static_cast<U>(value);
	if constexpr (std::is_signed_v<P>) if (power < P{})
	{
		v = m?(qr_inverse(v, m) + m) % m:qr_inverse_euclid(subtraction{}, euclidean_division{}, v, (~v + 1) % v, U{1}, ~((~v + 1) / v));
		power = -power;
	}
	using UP = std::make_unsigned_t<P>;
	auto p = static_cast<UP>(power);
	if (bsr(m) == bsf(m) && v & 1)
		p &= static_cast<UP>((m >> 1) - 1);
	else if (is_small_prime(m))
		p %= static_cast<UP>(m - 1);
	auto r = U{1};
	while (!!p)
	{
		if (p & 1)
			r = mul_mod(r, v, m);
		p >>= 1;
		v = mul_mod(v, v, m);
	}
	if (!!r && bsr(r) >= std::numeric_limits<T>::digits)
		throw std::overflow_error("Integer is overflow");
	return static_cast<T>(r);
}

template <std::integral T, std::integral K, std::integral M>
constexpr bool is_primitive_kth_root(T value, K k, M modulo)
{
	if constexpr (std::is_signed_v<K>)
		return is_primitive_kth_root(value, static_cast<std::make_unsigned_t<K>>(k < K{}?-k:k), modulo);
	else
	{
		if (value == T{1})
			return k == K{1};
		if (pow(value, k, modulo) != T{1})
			return false;
		for (auto prime:small_primes | std::views::take_while([k](auto p) {return p * p <= k;}))
		{
			if (!(k % prime) && pow(value, k / prime, modulo) == T{1})
				return false;
		}
		return true;
	}
}

template <std::unsigned_integral K, std::unsigned_integral M>
constexpr bool has_primitive_kth_root(K k, M modulo)
{
	return !!k && modulo != M{1} && eulers_totient(modulo) % k == 0;
}

struct no_roots:std::runtime_error
{
	no_roots():std::runtime_error("No root") {}
};

template <std::unsigned_integral K, std::unsigned_integral M>
constexpr M get_primitive_kth_root(K k, M modulo)
{
	if (!k || modulo == M{1})
		throw no_roots{};
	M lambda = modulo?carmichael(modulo):M{1} << (std::numeric_limits<M>::digits - 2);
	if (lambda % k != 0)
		throw no_roots{};
	for (auto x = M{1}; x != modulo; ++x)
		if (is_primitive_kth_root(x, lambda, modulo))
			return pow(x, lambda / k, modulo);
	throw no_roots{};
}
