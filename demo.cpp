#ifdef __GNUC__
#define __GLIBCXX_BITSIZE_INT_N_0 128
#define __GLIBCXX_TYPE_INT_N_0 __int128
#endif

#include <type_traits>
#include <iostream>
#include <iomanip>
#include <ranges>
#include <functional>

#include "bit_scan.h"
#include "as_binary.h"
#include "poly_div.h"
#include "int_ring.h"
#include "galois_field.h"
#include "groups.h"

int main(int argc, char** argv)
{
	std::cout << "bsf(" << as_binary(0) << ") = " << int(bsf(0)) << "\n";
	std::cout << "bsr(" << as_binary(0) << ") = " << int(bsr(0)) << "\n";
	std::cout << "bsf(" << as_binary(4) << ") = " << int(bsf(4)) << "\n";
	std::cout << "bsr(" << as_binary(4) << ") = " << int(bsr(4)) << "\n";
	std::cout << "bsf(" << as_binary(6) << ") = " << int(bsf(6)) << "\n";
	std::cout << "bsr(" << as_binary(6) << ") = " << int(bsr(6)) << "\n";
	std::cout << "bsf(" << as_binary(0x7fffffffffffff20lu) << ") = " << int(bsf(0x7fffffffffffff20lu)) << "\n";
	std::cout << "bsr(" << as_binary(0x7fffffffffffff20lu) << ") = " << int(bsr(0x7fffffffffffff20lu)) << "\n";
	std::cout << std::endl;

	std::cout << "In Z_2083 the value of (12 ^ -1 000 000 000) is " << pow(QuotientRingElement<unsigned, 2083>{12}, -1000000000) << '\n';
	std::cout << "In Z_{2^32} (type based) the value of (241 ^ -1 000 000 000) is " << pow(QuotientRingElement<unsigned>{241}, -1000000000) << '\n';
	std::cout << "In GF(2^13) with polynomial 0x201B the value of pow(123, 456789012) is " << pow(galois_element<unsigned, value_based_irreducable<unsigned, 0x201B>>{120}, 456789012) << '\n';
	std::cout << "In GF(2^32) with the default type polynomial the value of pow(0x12345678, -0x789ABCDE) is " << pow(galois_element<unsigned>{0x12345678}, -0x789ABCDE) << '\n';
	std::cout << std::endl;

	{
		using Z = QuotientRingElement<unsigned, 43>;
		std::cout << std::setw(7) << "Element" << '\t' << std::setw(7) << "Squared" << '\t' << "Root of squared\n";
		for (unsigned i = 0; i <= 43 / 2; ++i)
		{
			auto elem = Z(i);
			auto sq = elem * elem;
			std::cout << std::setw(7) << elem << '\t' << std::setw(7) << sq << '\t' << sqrt(sq) << '\n';
		}
		std::cout << std::setw(7) << "Element" << '\t' << std::setw(7) << "Squared" << '\t' << "Additive inverse of the root of squared\n";
		for (unsigned i = 43 - 43 / 2; i < 43; ++i)
		{
			auto elem = Z(i);
			auto sq = elem * elem;
			std::cout << std::setw(7) << elem << '\t' << std::setw(7) << sq << '\t' << -sqrt(sq) << '\n';
		}
		std::cout << std::endl;
	}
	{
		using GF = galois_element<unsigned, value_based_irreducable<unsigned, 0B10011>>;
		std::cout << "SQ2: ";
		for (unsigned i = 0; i < 1u << GF::polynomial::degree; ++i)
			std::cout << std::setw(2) << sqrt(GF(i)) << ' ';
		std::cout << "\nVAL: ";
		for (unsigned i = 0; i < 1u << GF::polynomial::degree; ++i)
			std::cout << std::setw(2) << GF(i) << ' ';
		std::cout << "\nPW2: ";
		for (unsigned i = 0; i < 1u << GF::polynomial::degree; ++i)
			std::cout << std::setw(2) << pow(GF(i), 2) << ' ';
		std::cout << '\n';
		std::cout << std::endl;
	}
	std::cout << "Default unsigned char based galois field polynomial: " << galois_element<unsigned char>::irreducable_polynomial << '\n';
	std::cout << "Default int based galois field polynomial: " << galois_element<int>::irreducable_polynomial << '\n';
	std::cout << std::endl;
	{
		using Z_7 = QuotientRingElement<unsigned, 7>;
		using gf256_0 = galois_element<unsigned char>;
		using gf256_1 = galois_element<unsigned char, value_based_irreducable<unsigned, 0b100011011>>;
		std::cout << "Reciprocal of 3 modulo 7 using the extended Euclidian algorithm: " << qr_inverse(3, 7) << "\n";
		std::cout << "Reciprocal of 3 in Z_7: " << Z_7{3}.reciprocal() << "\n";
		std::cout << "Reciprocal of 3 in GF(256) (default polynomial): " << static_cast<unsigned>(gf256_0(3).reciprocal()) << "\n";
		std::cout << "Reciprocal of 3 in GF(256) (polynomial: " << gf256_1::irreducable_polynomial << "): " << static_cast<unsigned>(gf256_1(3).reciprocal()) << "\n";
		std::cout << "Reciprocal of 3 in GF(2^32) (default polynomial): " << (3_ge).reciprocal() << "\n";

		std::cout << "5/4 in Z_7: " << Z_7{5} / Z_7{4} << "\n";
		std::cout << "5/4 in GF(256) (default polynomial " << gf256_0::irreducable_polynomial << "): " << static_cast<unsigned>(galois_element<unsigned char>(5) / galois_element<unsigned char>(4)) << "\n";
		std::cout << "5/4 in GF(256) (explicit polynomial: " << gf256_1::irreducable_polynomial << "): " << static_cast<unsigned>(gf256_1{5} / gf256_1{4}) << "\n";
		std::cout << std::endl;
	}
	{
		auto poly_cldiv_show = [](auto poly_int_val_1, auto poly_int_val_2)
		{
			auto dv = poly_div(poly_int_val_1, poly_int_val_2);
			std::cout << "(" << poly_display_t{poly_int_val_1} << ") / (" << poly_display_t{poly_int_val_2} << ") = "
				<< as_binary(poly_int_val_1) << " / " << as_binary(poly_int_val_2) << " = {"
				<< as_binary(dv.quot) << ", " << as_binary(dv.rem) << "}\n";
		};
		poly_cldiv_show(130, 131);
		poly_cldiv_show(131, 3);
		poly_cldiv_show(0x1084, 0x11b);
		std::cout << std::endl;
	}

	/*{
		using gf8 = galois_element<unsigned char, type_based_irreducable<std::uint8_t, 0xF5>>;
		std::cout << "GF(256):\n";
		for (unsigned i = 0; i < 0x100; ++i)
			std::cout << std::setw(2) << i << ' ';
		std::cout << "\nSquares:\n";
		for (unsigned i = 0; i < 0x100; ++i)
			std::cout << std::setw(2) << unsigned(gf8(i) * gf8(i)) << ' ';
		std::cout << '\n';
		gf8 x{0x70};
		std::cout << "x^3 + 11 * x + 12 = 0x" << unsigned(x * x * x + gf8(11) * x + gf8(12)) << '\n';
		std::cout << "148^2 = 0x" << unsigned(gf8(148) * gf8(148)) << '\n';

		std::cout << std::endl;
	}*/
	/*{
		using field = QuotientRingElement<unsigned, 101>;
		std::cout.setf(std::ios_base::dec, std::ios_base::basefield);
		std::cout << "Z_101:\n";
		for (unsigned i = 0; i < 101; ++i)
			std::cout << std::setw(3) << i << ' ';
		std::cout << "\nSquares:\n";
		for (unsigned i = 0; i < 101; ++i)
			std::cout << std::setw(3) << unsigned(field(i) * field(i)) << ' ';
		std::cout << '\n';
		field x{53};
		std::cout << "x^3 + 47 * x + 99 = " << unsigned(x * x * x + field(47) * x + field(99)) << '\n';
		std::cout << "13^2 = 0x" << unsigned(field(13) * field(13)) << '\n';

		using ec = ecc<field>;
		using ecp = ecc_point<ec>;
		ecp g = ecp(field(53), field(13), ec(field(47), field(99)));
		std::cout << g << '\n';
		std::cout << pow(g, 3) << '\n';
		std::cout << pow(g, 80) << '\n';
		std::cout << pow(pow(g, 3), 80) << ' ' << pow(pow(g, 80), 3) << ' ' << pow(g, 240) << '\n';


		std::cout << std::endl;
	}*/
	/*{
		std::cout << "Permutation subgroup";
		unsigned reg[8] = {1, 2, 3, 4, 5, 6, 7, 8};
		std::cout << "0: ";
		for (auto v:reg)
			std::cout << std::setw(2) << v;
		std::cout << '\n';
		for (int i = 1; i < 10; ++i)
		{
			auto tmp = reg[0];
			reg[0] = reg[1];
			reg[1] = reg[7];
			reg[7] = tmp;
			tmp = reg[2];
			reg[2] = reg[3];
			reg[3] = reg[6];
			reg[6] = tmp;
			tmp = reg[4];
			reg[4] = reg[5];
			reg[5] = tmp;
			std::cout << i << ": ";
			for (auto v:reg)
				std::cout << std::setw(2) << v;
			std::cout << '\n';
		}
		std::cout << std::endl;
	}*/
	{
		auto reciprocal = [](auto val) {return val.reciprocal();};
		std::cout << "Elements of Z20*: ";
		{
			constexpr multiplicative_integral_set<unsigned, 20> set{};
			for (auto elem:set)
				std::cout << elem << " ";
			std::cout << "\n";
		}
		std::cout << "==Z16 subgroups==\n";
		for (auto gen:Group{std::views::iota(0u, 16u) | std::views::transform([](auto i) {return QuotientRingElement<unsigned, 16u>{i};}), std::plus<>{}, std::negate<>{}})
		{
			std::cout << unsigned(gen) << ':';
			for (auto elem:Group{cyclic_set{gen, std::plus<>{}, std::negate{}}})
				std::cout << ' ' << elem;
			std::cout << "\n";
		}
		std::cout << "==Z16* subgroups==\n";
		for (auto gen:Group{std::views::iota(0u, 8u) | std::views::transform([](auto i) {return QuotientRingElement<unsigned, 16u>{1u + i + i};}), std::multiplies<>{}, reciprocal})
		{
			std::cout << unsigned(gen) << ':';
			for (auto elem:Group{cyclic_set{gen, std::multiplies<>{}, reciprocal}})
				std::cout << ' ' << elem;
			std::cout << "\n";
		}
		std::cout << "==Z23 subgroups==\n";
		{
			auto g = make_additive_integral_group<unsigned, 23u>();
			for (auto gen:g)
			{
				std::cout << unsigned(gen) << ':';
				for (auto elem:g.cyclic_subgroup(gen))
					std::cout << ' ' << elem;
				std::cout << "\n";
			}
		}
		std::cout << "==Z17* subgroups==\n";
		{
			auto g = make_multiplicative_integral_group<unsigned, 17u>();
			for (auto gen:g)
			{
				std::cout << unsigned(gen) << ':';
				for (auto elem:g.cyclic_subgroup(gen))
					std::cout << ' ' << elem;
				std::cout << "\n";
			}
		}
		std::cout << "==Z30 subgroups==\n";
		{
			auto g = make_additive_integral_group<unsigned, 30u>();
			for (auto gen:g)
			{
				std::cout << unsigned(gen) << ':';
				for (auto elem:g.cyclic_subgroup(gen))
					std::cout << ' ' << elem;
				std::cout << "\n";
			}
		}
		std::cout << "==Z30* subgroups==\n";
		{
			auto g = make_multiplicative_integral_group<unsigned, 30u>();
			for (auto gen:g)
			{
				std::cout << unsigned(gen) << ':';
				for (auto elem:g.cyclic_subgroup(gen))
					std::cout << ' ' << elem;
				std::cout << "\n";
			}
		}
		std::cout << "==GF(32) additive subgroups==\n";
		{
			using F = galois_element<unsigned, value_based_irreducable<unsigned, 0B1101101>>;
			auto g = Group{std::views::iota(0u, 32u) | std::views::transform([](auto i){return F{i};}), std::plus<>{}, std::negate<>{}};
			for (auto gen:g)
			{
				auto subgroup = g.cyclic_subgroup(gen);
				for (auto val:subgroup | std::views::take(subgroup.order() - 1))
					std::cout << val << "->";
				//std::cout << *std::ranges::rbegin(subgroup) << "; "; //fails in g++-10
				std:: cout << std::end(subgroup)[-1] << ';';
			}
			std::cout << std::endl;
		}
		std::cout << "==GF(32) multiplicative subgroups==\n";
		{
			using F = galois_element<unsigned, value_based_irreducable<unsigned, 0B1101101>>;
			auto g = Group{std::views::iota(1u, 32u) | std::views::transform([](auto i){return F{i};}), std::multiplies<>{}, reciprocal};
			for (auto gen:g)
			{
				std::cout << unsigned(gen) << ':';
				for (auto elem:g.cyclic_subgroup(gen))
					std::cout << ' ' << elem;
				std::cout << "\n";
			}
			std::cout << std::endl;
		}
	}

	std::cout << "reciprocal(7) in 2^64: " << QuotientRingElement<unsigned long long>(7).reciprocal() << '\n';

	std::cout.setf(std::ios_base::hex, std::ios_base::basefield);
	std::cout << "Primitive 2nd root in Z_(1<<16 + 1): 0x" << get_primitive_kth_root(2ull, 0x10001ull) << '\n';
	std::cout << "Primitive 4th root in Z_(1<<16 + 1): 0x" << get_primitive_kth_root(4ull, 0x10001ull) << '\n';
	std::cout << "Primitive 8th root in Z_(1<<16 + 1): 0x" << get_primitive_kth_root(8ull, 0x10001ull) << '\n';
	std::cout << "Primitive 16th root in Z_(1<<16 + 1): 0x" << get_primitive_kth_root(16ull, 0x10001ull) << '\n';
	auto root = get_primitive_kth_root(16ull, 0x10001ull), acc = 1ull;
	for (unsigned i = 0; i < 16; ++i, acc = mul_mod(acc, root, 0x10001ull))
		std::cout << "0x" << acc << ' ';
	std::cout << '\n';

	std::cout << "Primitive 2nd root in Z_(1<<32): 0x" << get_primitive_kth_root(2u, 0u) << '\n';
	std::cout << "Primitive 4th root in Z_(1<<32): 0x" << get_primitive_kth_root(4u, 0u) << '\n';
	std::cout << "Primitive 8th root in Z_(1<<32): 0x" << get_primitive_kth_root(8u, 0u) << '\n';

	std::cout << "Primitive 2nd root in Z_(1<<32 + 1): 0x" << get_primitive_kth_root(2ull, 0x100000001ull) << '\n';
	std::cout << "Primitive 4th root in Z_(1<<32 + 1): 0x" << get_primitive_kth_root(4ull, 0x100000001ull) << '\n';
	std::cout << "Primitive 8th root in Z_(1<<32 + 1): 0x" << get_primitive_kth_root(8ull, 0x100000001ull) << '\n';
	std::cout << "Primitive 8th root in Z_337: 0x" << get_primitive_kth_root(8u, 337u) << '\n';
	std::cout << "Primitive 8th root in (1<<32 + 2): 0x" << get_primitive_kth_root(7u, 0x100000002ull) << '\n';

	std::cout << "Primitive 2nd root in Z_(1<<64): 0x" << get_primitive_kth_root(2ull, 0ull) << '\n';
	std::cout << "Primitive 4th root in Z_(1<<64): 0x" << get_primitive_kth_root(4ull, 0ull) << '\n';
	//std::cout << "Primitive 7th root in Z_(1<<63 + 1): 0x" << get_primitive_kth_root(7ull, 0x8000000000000001ull) << '\n';
	std::cout << "3**3317822978904 in Z_(1<<63 + 1): 0x" << pow(QuotientRingElement<unsigned long long, 0x8000000000000001ull>(3), 3317822978904) << '\n';
	std::cout << "Primitive 8th root in Z_(1<<64): 0x" << get_primitive_kth_root(8ull, 0ull) << '\n';

	{
		std::cout << std::dec;
		std::cout << "Elements of Z36*: ";
		{
			constexpr multiplicative_integral_set<unsigned, 36> set{};
			for (auto elem:set)
				std::cout << elem << " ";
			std::cout << "\n";
		}
		std::cout << "==Z36* subgroups==\n" << std::dec;
		{
			auto g = make_multiplicative_integral_group<unsigned, 36u>();
			for (auto gen:g)
			{
				std::cout << unsigned(gen) << ':';
				for (auto elem:g.cyclic_subgroup(gen))
					std::cout << ' ' << elem;
				std::cout << "\n";
			}
		}
	}

	return 0;
}
