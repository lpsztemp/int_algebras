#include <type_traits>
#include <array>
#include <cstdint>
#include <climits>

#ifndef ABS_ALG_BIT_SCAN_H_
#define ABS_ALG_BIT_SCAN_H_

#if CHAR_BIT != 8
#error 8-bit char is required.
#endif

namespace details {

template <class BaseType, BaseType Value>
struct bsr_4_t:std::integral_constant<BaseType, (Value & 15) >= 8?4:((Value & 15) >= 4?3:((Value & 15) >= 2?2:((Value == 1)?1:0)))> {};
template <class BaseType, BaseType Value>
struct bsr_8_t:std::integral_constant<BaseType, bsr_4_t<BaseType, (Value >> 4)>::value == 0?bsr_4_t<BaseType, Value>::value:
	(bsr_4_t<BaseType, (Value >> 4)>::value?bsr_4_t<BaseType, (Value >> 4)>::value + 4:0)> {};
constexpr std::array<std::uint8_t, 16> bsf_4bit_array = {0, 1, 2, 1, 3, 1, 2, 1, 4, 1, 2, 1, 3, 1, 2, 1};
template <class BaseType, BaseType Value>
struct bsf_4_t:std::integral_constant<BaseType, bsf_4bit_array[Value & 15]> {};
template <class BaseType, BaseType Value>
struct bsf_8_t:std::integral_constant<BaseType, bsf_4_t<BaseType, (Value & 0xF)>::value == 0?
	(bsf_4_t<BaseType, (Value >> 4)>::value?bsf_4_t<BaseType, (Value >> 4)>::value + 4:0):bsf_4_t<BaseType, Value>::value> {};

template <std::size_t i = 0, std::uint8_t ... first_part>
constexpr auto init_bsr_byte_array() noexcept -> std::enable_if_t<(i == 255), std::array<std::uint8_t, 256>>
{
	return std::array<std::uint8_t, 256>{first_part..., bsr_8_t<std::uint8_t, i>::value};
}
template <std::size_t i = 0, std::uint8_t ... first_part>
constexpr auto init_bsr_byte_array() noexcept -> std::enable_if_t<(i < 255), std::array<std::uint8_t, 256>>
{
	return init_bsr_byte_array<i + 1, first_part..., bsr_8_t<std::uint8_t, i>::value>(); 
}

constexpr std::array<std::uint8_t, 256> bsr_byte_array = init_bsr_byte_array();

template <std::size_t i = 0, std::uint8_t ... first_part>
constexpr auto init_bsf_byte_array() noexcept -> std::enable_if_t<(i == 255), std::array<std::uint8_t, 256>>
{
	return std::array<std::uint8_t, 256>{first_part..., bsf_8_t<std::uint8_t, i>::value};
}
template <std::size_t i = 0, std::uint8_t ... first_part>
constexpr auto init_bsf_byte_array() noexcept -> std::enable_if_t<(i < 255), std::array<std::uint8_t, 256>>
{
	return init_bsf_byte_array<i + 1, first_part..., bsf_8_t<std::uint8_t, i>::value>(); 
}

constexpr std::array<std::uint8_t, 256> bsf_byte_array = init_bsf_byte_array();

template <class T>
constexpr std::uint8_t get_byte(T value, std::uint8_t i)
{
	using uT = std::make_unsigned_t<T>;
	return i >= sizeof(T) * 8?std::uint8_t(0):std::uint8_t((uT(value) >> (i * 8)) & 0xff);
}

template <std::uint8_t i = 0, class T>
constexpr auto bsf_impl(T value) -> std::enable_if_t<(i >= sizeof(T) - 1), std::uint8_t>
{
	return bsf_byte_array[get_byte(value, sizeof(T) - 1)] + (sizeof(T) - 1) * 8;
}
template <std::uint8_t i = 0, class T>
constexpr auto bsf_impl(T value) -> std::enable_if_t<(i < sizeof(T) -1), std::uint8_t>
{
	auto result = bsf_byte_array[get_byte(value, i)];
	return result == 0?bsf_impl<i + 1>(value):result + i * 8;
}

template <std::uint8_t i = 0, class T>
constexpr auto bsr_impl(T value) -> std::enable_if_t<(i >= sizeof(T) - 1), std::uint8_t>
{
	return bsr_byte_array[get_byte(value, 0)];
}
template <std::uint8_t i = 0, class T>
constexpr auto bsr_impl(T value) -> std::enable_if_t<(i < sizeof(T) - 1), std::uint8_t>
{
	auto result = bsr_byte_array[get_byte(value, sizeof(T) - 1 - i)];
	return result == 0?bsr_impl<i + 1>(value):result + (sizeof(T) - 1 - i) * 8;
}

} //details

template <class T>
constexpr std::uint8_t bsf(T val) noexcept
{
	using namespace details;
	return val == T()?sizeof(T) * 8:bsf_impl(val) - 1;
}

template <class T>
constexpr std::uint8_t bsr(T val) noexcept
{
	using namespace details;
	using uT = std::make_unsigned_t<T>;
	return val == T()?sizeof(T) * 8:bsr_impl(val) - 1;
}

#endif //ABS_ALG_BIT_SCAN_H_
