#include "gcd.h"
#include <vector>
#include <array>
#include <concepts>
#include <ranges>
#include <functional>
#include <utility>

#ifndef ABS_ALG_GROUPS_H_
#define ABS_ALG_GROUPS_H_

template <class ElementType, ElementType modulo>
class additive_integral_set
{
	static constexpr auto view = std::ranges::iota_view{ElementType{}, modulo} | std::views::transform([](auto i) {return i % modulo;});
public:
	using value_type = ElementType;
	constexpr ElementType operator[](std::size_t i) const noexcept
	{
		return ElementType(i) % modulo;
	}
	constexpr std::size_t size() const noexcept
	{
		return std::size_t(modulo);
	}
	constexpr auto begin() const {return view.begin();}
	constexpr auto end() const {return view.end();}
};

template <class ElementType, ElementType modulo>
class multiplicative_integral_set
{
	template <ElementType i, ElementType ... first_values>
	static constexpr auto m_get_array(std::enable_if_t<(i < modulo) && gcd(modulo, i) == 1, int>) noexcept
	{
		return m_get_array<i + 1, first_values..., i>(0);
	}
	template <ElementType i, ElementType ... first_values>
	static constexpr auto m_get_array(std::enable_if_t<(i < modulo) && gcd(modulo, i) != 1, int>) noexcept
	{
		return m_get_array<i + 1, first_values...>(0);
	}
	template <ElementType i, ElementType ... first_values>
	static constexpr auto m_get_array(std::enable_if_t<(i >= modulo), int>) noexcept
	{
		return std::array<ElementType, sizeof...(first_values)>{first_values...};
	}
	static constexpr auto v = m_get_array<0>(0);
	constexpr static std::size_t count = std::size(v);
public:
	using value_type = ElementType;
	multiplicative_integral_set() = default;
	constexpr ElementType operator[](std::size_t i) const noexcept
	{
		return v[i];
	}
	constexpr std::size_t size() const noexcept
	{
		return count;
	}
	constexpr auto begin() const {return v.begin();}
	constexpr auto end() const {return v.end();}
};

template <std::equality_comparable T, class Op, class Inv>
	requires std::is_invocable_r_v<T, Op, T, T> && std::is_invocable_r_v<T, Inv, T>
class cyclic_set
{
	T m_gen;
	Op m_op = Op{};
	Inv m_inv = Inv{};
	constexpr std::size_t count_elements(T gen)
	{
		std::size_t count = 0u;
		T val = gen;
		do
		{
			++count;
			val = m_op(val, gen);
		}while (val != gen);
		return count;
	}
	std::size_t m_count = count_elements(m_gen);

	struct MapIndexToValue
	{
		const cyclic_set* m_ptr;
		constexpr T operator()(std::size_t i) const
		{
			return (*m_ptr)[i];
		}
	};
	std::ranges::transform_view<std::ranges::iota_view<std::size_t, std::size_t>, MapIndexToValue> view 
		= std::ranges::iota_view{std::size_t{}, m_count} | std::views::transform(MapIndexToValue{this});
public:
	constexpr const Op& get_op_() const noexcept
	{
		return m_op;
	}
	constexpr const Inv& get_inv_() const noexcept
	{
		return m_inv;
	}
public:
	constexpr cyclic_set(T gen, Op group_operation, Inv inverse_operation, std::size_t count) noexcept(std::is_nothrow_copy_constructible_v<T>)
		:m_gen(gen), m_op(group_operation), m_inv(inverse_operation), m_count(count) {}
	constexpr explicit cyclic_set(T gen, Op group_operation = Op{}, Inv inverse_operation = Inv{}) noexcept(std::is_nothrow_copy_constructible_v<T>)
		:m_gen(gen), m_op(group_operation), m_inv(inverse_operation), m_count(count_elements(m_gen)) {}
	constexpr cyclic_set(const cyclic_set& right):cyclic_set(right.m_gen, right.m_op, right.m_inv, right.m_count) {}
	constexpr cyclic_set& operator=(const cyclic_set& right)
	{
		if (this != &right)
		{
			m_gen = right.m_gen;
			m_op = right.m_op;
			m_inv = right.m_inv;
			m_count = right.m_count;
			view = std::ranges::iota_view{std::size_t{}, m_count} | std::views::transform(MapIndexToValue{this});
		}
		return *this;
	}
	constexpr T operator[](std::size_t i) const
	{
		T acc = m_gen, result = m_op(m_gen, m_inv(m_gen));
		while (i > 0)
		{
			if (i & 1)
				result = m_op(result, acc);
			acc = m_op(acc, acc);
			i >>= 1;
		}
		return result;
	}
	constexpr std::size_t size() const noexcept
	{
		return m_count;
	}
	constexpr auto begin() const {return view.begin();}
	constexpr auto end() const {return view.end();}
};

template <class T>
concept Set = std::ranges::sized_range<T> && requires (T x) {x[0]; x.size();};

template <Set SetType, class GroupOperation, class GroupInverse>
	requires std::is_invocable_r_v<decltype(std::declval<SetType>()[0]), GroupOperation, decltype(std::declval<SetType>()[0]), decltype(std::declval<SetType>()[0])> &&
			std::is_invocable_r_v<decltype(std::declval<SetType>()[0]), GroupInverse, decltype(std::declval<SetType>()[0])>
class Group;

template <class InputSetType, class GroupOperation, class GroupInverse>
Group(InputSetType set, GroupOperation op, GroupInverse) -> Group<InputSetType, GroupOperation, GroupInverse>;

template <class ElementType, class GroupOperation, class GroupInverse>
Group(cyclic_set<ElementType, GroupOperation, GroupInverse>) -> Group<cyclic_set<ElementType, GroupOperation, GroupInverse>, GroupOperation, GroupInverse>;

template <Set SetType, class GroupOperation, class GroupInverse>
	requires std::is_invocable_r_v<decltype(std::declval<SetType>()[0]), GroupOperation, decltype(std::declval<SetType>()[0]), decltype(std::declval<SetType>()[0])> &&
			std::is_invocable_r_v<decltype(std::declval<SetType>()[0]), GroupInverse, decltype(std::declval<SetType>()[0])>
class Group
{
	SetType m_v;
	GroupOperation m_op;
	GroupInverse m_inv;
public:
	using value_type = decltype(std::declval<SetType>()[0]);
public:
	Group() = default;
	template <class InputSetType>
	explicit constexpr Group(InputSetType&& set, const GroupOperation& group_operation = GroupOperation{}, const GroupInverse& group_inverse = GroupInverse{})
		:m_v(std::forward<InputSetType>(set)), m_op(group_operation), m_inv(group_inverse) {}
	template <class ElementType>
	explicit constexpr Group(cyclic_set<ElementType, GroupOperation, GroupInverse> underlying_set):m_v(underlying_set), m_op(m_v.get_op_()), m_inv(m_v.get_inv_()) {}
	constexpr std::size_t order() const
	{
		return m_v.size();
	}
	constexpr std::size_t size() const
	{
		return m_v.size();
	}
	constexpr value_type operator[](std::size_t i) const
	{
		return m_v[i];
	}
	constexpr auto begin() const
	{
		return std::begin(m_v);
	}
	constexpr auto end() const
	{
		return std::end(m_v);
	}
	constexpr value_type operator()(value_type left, value_type right) const
	{
		return m_op(left, right);
	}
	constexpr value_type inverse(value_type value) const
	{
		return m_inv(value);
	}
	constexpr auto cyclic_subgroup(value_type generator) const
	{
		return Group<cyclic_set<value_type, GroupOperation, GroupInverse>, GroupOperation, GroupInverse>{cyclic_set{generator, m_op, m_inv}};
	}
};

template <class SetType, class ... SetConstructionParams>
	requires (std::constructible_from<SetType, SetConstructionParams&&...>)
auto make_additive_group_of(SetConstructionParams&& ... set_construction_args)
{
	return Group<SetType, std::plus<void>, std::negate<void>>(SetType(std::forward<SetConstructionParams>(set_construction_args)...));
}

template <class SetType, class ... SetConstructionParams>
	requires (std::constructible_from<SetType, SetConstructionParams&&...>)
auto make_multiplicative_group_of(SetConstructionParams&& ... set_construction_args)
{
	return Group(SetType(std::forward<SetConstructionParams>(set_construction_args)...), std::multiplies<void>(), [](auto val){return val.reciprocal();});
}

template <class ElementType, ElementType GroupOrder> requires (GroupOrder >= 0)
constexpr auto make_additive_integral_group() noexcept
{
	return Group{additive_integral_set<ElementType, GroupOrder>{}, [](auto x, auto y) {return (x + y) % GroupOrder;}, [](auto val) {return GroupOrder - val;}};
}
template <class ElementType, ElementType GroupOrder>
constexpr auto make_multiplicative_integral_group() noexcept
{
	return Group{multiplicative_integral_set<ElementType, GroupOrder>{}, [](auto x, auto y) {return (x * y) % GroupOrder;}, [](auto val) {return qr_inverse(val, GroupOrder) % GroupOrder;}};
}

#endif //ABS_ALG_GROUPS_H_
