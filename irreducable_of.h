#include <type_traits>
#include <cstdint>
#include <stdexcept>
#include <iostream>
#include "bit_scan.h"
#include "poly_div.h"

#ifndef ABS_ALG_IRREDUCABLE_OF_H_
#define ABS_ALG_IRREDUCABLE_OF_H_

#if CHAR_BIT != 8
#error 8-bit char is required.
#endif

struct factors_set
{
	unsigned count;
	unsigned elements[5];
};

template <class BaseType, BaseType value>
struct type_based_irreducable
{
	static constexpr std::size_t degree = sizeof(BaseType) * CHAR_BIT;
	using base_type = BaseType;
	static constexpr BaseType adjust_value(BaseType val) noexcept
	{
		return val;
	}
	static constexpr BaseType left_shift(BaseType val) noexcept
	{
		return (val & (BaseType(1) << (degree - 1))) == 0?val << 1:(val << 1) ^ value;
	}
};

template <class T, T poly>
constexpr poly_div_t<T> poly_div(T dividend, type_based_irreducable<T, poly> divisor)
{
	return poly_div_t<T>{T(), dividend};
}

template <class T, T poly>
constexpr poly_div_t<T> poly_div(type_based_irreducable<T, poly> dividend, T divisor)
{
	if (divisor == T())
		throw poly_invalid_by_zero();
	auto q = T(), r = poly;
	auto dividend_deg = type_based_irreducable<T, poly>::degree;
	auto divisor_deg = bsr(divisor);
	do
	{
		auto off = (dividend_deg - divisor_deg);
		q |= T(1) << off;
		r ^= divisor << off;
		dividend_deg = bsr(r);
	}while (dividend_deg < sizeof(T) * 8 && dividend_deg >= divisor_deg);
	return poly_div_t<T>{q, r};
}

template <class TraitsType, class T, T value>
std::basic_ostream<char, TraitsType>& operator<<(std::basic_ostream<char, TraitsType>& os, type_based_irreducable<T, value>)
{
	os << "x^" << type_based_irreducable<T, value>::degree;
	auto val = value;
	while (val != 0)
	{
		auto deg = bsr(val);
		os << " + x^" << unsigned(deg);
		val &= ~(1 << deg);
	}
	return os;
}

template <class BaseType, BaseType value>
struct value_based_irreducable
{
	static constexpr std::size_t degree = bsr(value);
	using base_type = BaseType;
	static constexpr BaseType adjust_value(BaseType val) noexcept
	{
		return val < (1 << (degree - 1))?val:poly_div(val, value).rem;
	}
	static constexpr BaseType left_shift(BaseType val) noexcept
	{
		return (val & (1 << (degree - 1))) == 0?val << 1:(val << 1) ^ value;
	}
};

template <class TraitsType, class T, T value>
std::basic_ostream<char, TraitsType>& operator<<(std::basic_ostream<char, TraitsType>& os, value_based_irreducable<T, value>)
{
	os << "x^" << value_based_irreducable<T, value>::degree;
	auto val = value & ~(1 << value_based_irreducable<T, value>::degree);
	while (val != 0)
	{
		auto deg = bsr(val);
		os << " + x^" << unsigned(deg);
		val &= ~(1 << deg);
	}
	return os;
}

template <class T, T poly>
constexpr poly_div_t<T> poly_div(T dividend, value_based_irreducable<T, poly>)
{
	return poly_div(dividend, poly);
}

template <class T, T poly>
constexpr poly_div_t<T> poly_div(value_based_irreducable<T, poly>, T divisor)
{
	return poly_div(poly, divisor);
}

template <class T, std::size_t N>
using is_integral_of_bitsize = std::conjunction<std::is_integral<T>, std::integral_constant<bool, sizeof(T) * CHAR_BIT == N>>;
template <class T, std::size_t N>
constexpr bool is_integral_of_bitsize_v = is_integral_of_bitsize<T, N>::value;

template <class BaseType>
using irreducable_of =
	std::conditional_t<is_integral_of_bitsize_v<BaseType, 8>, type_based_irreducable<BaseType, 0x1B>,
	std::conditional_t<is_integral_of_bitsize_v<BaseType, 16>, type_based_irreducable<BaseType, 0x2B>,
	std::conditional_t<is_integral_of_bitsize_v<BaseType, 32>, type_based_irreducable<BaseType, 0x8D>,
	std::conditional_t<is_integral_of_bitsize_v<BaseType, 64>, type_based_irreducable<BaseType, 0x1B>,
	void>>>>;

struct invalid_polynomial_degree:std::runtime_error
{
	invalid_polynomial_degree():std::runtime_error("Invalid polynomial degree") {}
};

template <unsigned char i = 0, class BaseType, class BoolType_i, class ... BoolType_rest>
constexpr BaseType make_polynomial_code_impl(BaseType val, BoolType_i bit_i, BoolType_rest ... bits_rest)
{
	auto nv = BaseType(bit_i == BoolType_i()?val:BaseType(val | (1u << (sizeof(BaseType) * 8 - i - 1))));
	if constexpr (i == sizeof(BaseType) * 8 - 1)
		return nv | bit_i;
	else
		return make_polynomial_code_impl<i + 1>(nv, bits_rest...);
}

template <class BaseType, class BoolType0, class ... BoolType>
constexpr auto make_irreducable_polynomial(BoolType0 first, BoolType ... bits)
	-> std::enable_if_t<(sizeof...(BoolType) > 0), BaseType>
{
	static_assert(sizeof...(bits) == sizeof(BaseType) * 8);
	if (!first)
		throw invalid_polynomial_degree();
	return make_polynomial_code_impl<0>(BaseType(), bits...);
}

template <class BaseType, class T>
constexpr BaseType make_irreducable_polynomial(T code)
{
	if (bsr(code) != sizeof(BaseType) * 8)
		throw invalid_polynomial_degree();
	return BaseType(code & (T(-1) >> 1));
}

template <class T>
struct poly_display_t
{
	T val;
};

template <class T>
poly_display_t(T val) -> poly_display_t<T>;

template <class TraitsType, class T>
std::basic_ostream<char, TraitsType>& operator<<(std::basic_ostream<char, TraitsType>& os, poly_display_t<T> tagged_value)
{
	if (!tagged_value.val)
	{
		os << 0;
		return os;
	}
	using uT = std::make_unsigned_t<T>;
	auto deg = unsigned(bsr(tagged_value.val));
	os << "x^" << deg;
	auto val = static_cast<uT>(tagged_value.val) << (sizeof(uT) * CHAR_BIT - deg);
	constexpr auto msk = static_cast<uT>(uT(1) << (sizeof(uT) * CHAR_BIT - 1));
	while (val != 0)
	{
		--deg;
		if ((val & msk) != 0)
			os << " + x^" << deg;
		val += val;
	}
	return os;
}

template <class T> auto poly_display(T val) noexcept
{
	return poly_display_t<T>{val};
}

#endif //ABS_ALG_IRREDUCABLE_OF_H_
