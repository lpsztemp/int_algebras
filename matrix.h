#pragma once

#include <cstdlib>
#include <vector>
#include <iterator>
#include <stdexcept>
#include <initializer_list>
#include <utility>
#include <cassert>
#include <algorithm>
#include <concepts>
#include "generics.h"

struct invalid_matrix_row_index:std::out_of_range {
	inline invalid_matrix_row_index():std::out_of_range("Matrix row index is out of range") {}
};

struct invalid_matrix_dimensions:std::invalid_argument {
	inline invalid_matrix_dimensions():std::invalid_argument("Matrix dimensions are not supported by the operation") {}
};

struct singular_matrix_inversion:std::domain_error {
	inline singular_matrix_inversion():std::domain_error("Inversion of a singular matrix") {}
};

template <class ValueType>
class matrix;

template <class ValueType>
class matrix_column
{
	matrix<ValueType>* m_host_matrix;

	friend class matrix<ValueType>;
	matrix_column(matrix<ValueType>* host_matrix):m_host_matrix(host_matrix) {}
	matrix_column(const matrix_column&) = default;
	matrix_column& operator=(const matrix_column&) = default;
public:
	typedef ValueType value_type, *pointer, &reference;
	typedef const ValueType *const_pointer, &const_reference;
	typedef std::size_t size_type;
	typedef std::ptrdiff_t difference_type;

	matrix_column() = default;

	std::size_t rows() const noexcept;
	const ValueType* data() const noexcept;
	ValueType* data() noexcept;
	std::size_t size() const noexcept;
	const ValueType& operator[](std::size_t off) const noexcept
	{
		return this->data()[off];
	}
	ValueType& operator[](std::size_t off) noexcept
	{
		return this->data()[off];
	}
	const ValueType& at(std::size_t off) const
	{
		if (off > this->rows())
			throw invalid_matrix_row_index();
		return this->data()[off];
	}
	ValueType& at(std::size_t off)
	{
		if (off > this->rows())
			throw invalid_matrix_row_index();
		return this->data()[off];
	}
	typedef pointer iterator;
	typedef const_pointer const_iterator;
	typedef std::reverse_iterator<iterator> reverse_iterator;
	typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

	iterator begin() noexcept
	{
		return this->data();
	}
	const_iterator begin() const noexcept
	{
		return this->data();
	}
	const_iterator cbegin() const noexcept
	{
		return this->data();
	}
	iterator end() noexcept
	{
		return this->data() + this->rows();
	}
	const_iterator end() const noexcept
	{
		return this->data() + this->rows();
	}
	const_iterator cend() const noexcept
	{
		return this->data() + this->rows();
	}
	reverse_iterator rbegin() noexcept
	{
		return std::make_reverse_iterator(this->end());
	}
	const_reverse_iterator rbegin() const noexcept
	{
		return std::make_reverse_iterator(this->cend());
	}
	const_reverse_iterator crbegin() const noexcept
	{
		return std::make_reverse_iterator(this->cend());
	}
	reverse_iterator rend() noexcept
	{
		return std::make_reverse_iterator(this->begin());
	}
	const_reverse_iterator rend() const noexcept
	{
		return std::make_reverse_iterator(this->cbegin());
	}
	const_reverse_iterator crend() const noexcept
	{
		return std::make_reverse_iterator(this->cbegin());
	}
	bool empty() const noexcept
	{
		return this->size() == 0u;
	}
};

template <class ValueType>
class matrix
{
	friend class matrix_column<ValueType>;
	std::size_t m_columns = 0u, m_rows = 0u;
	std::vector<ValueType> m_elements;
	struct copyable_column: matrix_column<ValueType>
	{
		using matrix_column<ValueType>::matrix_column;
	};
	std::vector<copyable_column> m_column_data;
	template <class BaseIterator, class BaseIteratorValueType>
	class column_vector_iterator
	{
		using base_iterator = BaseIterator;
		base_iterator m_it;
	public:
		typedef BaseIteratorValueType value_type, *pointer, &reference;
		static_assert(std::is_same_v<reference, matrix_column<ValueType>&> || std::is_same_v<reference, const matrix_column<ValueType>&>);
		using difference_type = std::ptrdiff_t;
		using iterator_category = std::random_access_iterator_tag;
		column_vector_iterator() = default;
		column_vector_iterator(base_iterator it) noexcept(std::is_nothrow_copy_constructible_v<BaseIterator>) :m_it(it) {}
		template <class OtherBaseIterator, class = std::enable_if_t<std::is_convertible_v<OtherBaseIterator, base_iterator>>>
		column_vector_iterator(const OtherBaseIterator& it) noexcept(std::is_nothrow_constructible_v<BaseIterator, OtherBaseIterator>) :m_it(it) {}
		template <class OtherBaseIterator, class OtherValueType, class = std::enable_if_t<std::is_convertible_v<OtherBaseIterator, base_iterator>>>
		column_vector_iterator(const column_vector_iterator<OtherBaseIterator, OtherValueType>& it) noexcept(std::is_nothrow_constructible_v<BaseIterator, OtherBaseIterator>) :m_it(it.m_it) {}
		reference operator*() const
		{
			return *m_it;
		}
		pointer operator->() const
		{
			return &*m_it;
		}
		reference operator[](difference_type n) const
		{
			return m_it[n];
		}
		column_vector_iterator& operator++()
		{
			++m_it;
			return *this;
		}
		column_vector_iterator operator++(int)
		{
			auto old = *this;
			++m_it;
			return old;
		}
		column_vector_iterator& operator--()
		{
			--m_it;
			return *this;
		}
		column_vector_iterator operator--(int)
		{
			auto old = *this;
			--m_it;
			return old;
		}
		column_vector_iterator& operator+=(difference_type n)
		{
			m_it += n;
			return *this;
		}
		column_vector_iterator operator+(difference_type n) const
		{
			return column_vector_iterator{m_it + n};
		}
		column_vector_iterator& operator-=(difference_type n)
		{
			m_it -= n;
			return *this;
		}
		column_vector_iterator operator-(difference_type n) const
		{
			return column_vector_iterator{m_it - n};
		}
		difference_type operator-(const column_vector_iterator& other) const
		{
			return m_it - other.m_it;
		}
		auto operator<=>(const column_vector_iterator& right) const = default;
	};
public:
	typedef ValueType value_type, *pointer, &reference;
	typedef const ValueType *const_pointer, &const_reference;
	typedef std::size_t size_type;
	typedef std::ptrdiff_t difference_type;

	matrix() = default;
	inline matrix(std::size_t cols, std::size_t rows):m_columns(cols), m_rows(rows), m_elements(std::vector<ValueType>(m_columns * m_rows))
	{
		m_column_data.insert(m_column_data.end(), m_columns, copyable_column(this));
	}
	explicit inline matrix(const ValueType& diagonal_element, std::size_t cols, std::size_t rows):matrix(cols, rows) {
		std::size_t sq = std::min(cols, rows);
		for (std::size_t c = 0; c < sq; ++c)
			m_elements[c * m_rows + c] = diagonal_element;
	}
	inline matrix(std::initializer_list<std::initializer_list<ValueType>> elements)
	{
		m_rows = elements.size();
		if (!m_rows)
			m_columns = 0u;
		else
		{
			m_columns = elements.begin()->size();
			m_elements = std::vector<ValueType>(m_columns * m_rows);
			m_column_data.insert(m_column_data.end(), m_columns, copyable_column(this));
			for (std::size_t row = 0u; row < elements.size(); ++row)
			{
				std::size_t column_count = (elements.begin() + row)->size();
				if (column_count != m_columns)
					throw invalid_matrix_dimensions();
				for (std::size_t column = 0u; column < column_count; ++column)
					(*this)[column][row] = *((elements.begin() + row)->begin() + column);
			}
		}
	}
	matrix(const matrix& right):m_columns(right.m_columns), m_rows(right.m_rows), m_elements(right.m_elements)
	{
		m_column_data.insert(m_column_data.end(), m_columns, this);
	}
	matrix(matrix&& right):m_columns(right.m_columns), m_rows(right.m_rows), m_elements(std::move(right.m_elements))
	{
		m_column_data.insert(m_column_data.end(), m_columns, this);
		right.m_columns = right.m_rows = 0u;
		right.m_column_data.clear();
	}
	matrix(const ValueType* data, std::size_t columns, std::size_t rows):m_columns(columns), m_rows(rows), m_elements(data, data + columns * rows)
	{
		m_column_data.insert(m_column_data.end(), m_columns, this);
	}
	matrix(const std::vector<ValueType>& v, std::size_t columns, std::size_t rows):m_columns(columns), m_rows(rows), m_elements(v)
	{
		assert(m_elements.size() == columns * rows);
		m_column_data.insert(m_column_data.end(), m_columns, this);
	}
	matrix(std::vector<ValueType>&& v, std::size_t columns, std::size_t rows):m_columns(columns), m_rows(rows), m_elements(std::move(v))
	{
		assert(m_elements.size() == columns * rows);
		m_column_data.insert(m_column_data.end(), m_columns, this);
	}
	matrix(const std::vector<ValueType>& v):matrix(v, 1u, v.size()) {}
	matrix(std::vector<ValueType>&& v):matrix(std::move(v), 1u, v.size()) {}
	template <class IteratorBegin, class IteratorEnd,
		class = std::enable_if_t<std::is_constructible_v<ValueType, typename std::iterator_traits<IteratorBegin>::reference> &&
		std::is_same_v<std::remove_cv_t<typename std::iterator_traits<IteratorBegin>::value_type>, std::remove_cv_t<typename std::iterator_traits<IteratorEnd>::value_type>>>>
	matrix(IteratorBegin first, IteratorEnd last, std::size_t columns, std::size_t rows):m_columns(columns), m_rows(rows)
	{
		m_elements.reserve(columns * rows);
		while (first != last)
			m_elements.emplace_back(*first++);
		m_column_data.insert(m_column_data.end(), m_columns, this);
		assert(m_elements.size() == columns * rows);
	}
	template <class IteratorBegin, class IteratorEnd,
		class = std::enable_if_t<std::is_constructible_v<ValueType, typename std::iterator_traits<IteratorBegin>::reference> &&
		std::is_same_v<std::remove_cv_t<typename std::iterator_traits<IteratorBegin>::value_type>, std::remove_cv_t<typename std::iterator_traits<IteratorEnd>::value_type>>>>
	matrix(IteratorBegin first, IteratorEnd last):m_columns(1u)
	{
		if constexpr (std::is_base_of_v<std::random_access_iterator_tag, typename std::iterator_traits<IteratorBegin>::iterator_category>)
			m_elements.reserve(last - first);
		while (first != last)
			m_elements.emplace_back(*first++);
		m_rows = m_elements.size();
		m_column_data.insert(m_column_data.end(), m_columns, this);
	}
	template <class OtherValueType, class = std::enable_if_t<std::is_constructible_v<ValueType, OtherValueType>>>
	explicit matrix(const std::vector<OtherValueType>& v, std::size_t columns, std::size_t rows):matrix(v.begin(), v.end(), columns, rows) {}
	template <class OtherValueType, class = std::enable_if_t<std::is_constructible_v<ValueType, OtherValueType>>>
	explicit matrix(const std::vector<OtherValueType>& v):matrix(v.begin(), v.end()) {}
	matrix& operator=(const matrix& right)
	{
		if (this == &right)
			return *this;
		m_elements = right.m_elements;
		if (m_columns < right.m_columns)
			m_column_data.insert(m_column_data.end(), right.m_columns - m_columns, copyable_column(this));
		else if (m_columns > right.m_columns)
			m_column_data.erase(m_column_data.end() - (m_columns - right.m_columns), m_column_data.end());
		m_columns = right.m_columns;
		m_rows = right.m_rows;
		return *this;
	}
	matrix& operator=(matrix&& right)
	{
		if (this == &right)
			return *this;
		m_elements = std::move(right.m_elements);
		if (m_columns < right.m_columns)
			m_column_data.insert(m_column_data.end(), right.m_columns - m_columns, copyable_column(this));
		else if (m_columns > right.m_columns)
			m_column_data.erase(m_column_data.end() - (m_columns - right.m_columns), m_column_data.end());
		m_columns = right.m_columns;
		m_rows = right.m_rows;
		right.m_columns = 0u;
		right.m_rows = 0;
		return *this;
	}
	inline matrix_column<ValueType>& operator[](std::size_t column_index) noexcept
	{
		return m_column_data[column_index];
	}
	inline const matrix_column<ValueType>& operator[](std::size_t column_index) const noexcept
	{
		return m_column_data[column_index];
	}
	inline matrix_column<ValueType>& at(std::size_t column_index) noexcept
	{
		return m_column_data.at(column_index);
	}
	inline const matrix_column<ValueType>& at(std::size_t column_index) const noexcept
	{
		return m_column_data.at(column_index);
	}
	inline const ValueType* data() const noexcept
	{
		return m_elements.data();
	}
	inline ValueType* data() noexcept
	{
		return m_elements.data();
	}
	inline std::size_t size() const noexcept
	{
		return m_columns * m_rows;
	}
	inline std::size_t columns() const noexcept
	{
		return m_columns;
	}
	inline std::size_t rows() const noexcept
	{
		return m_rows;
	}
	bool empty() const noexcept
	{
		return m_columns == 0u;
	}

	typedef column_vector_iterator<typename std::vector<copyable_column>::iterator, matrix_column<ValueType>> iterator;
	typedef column_vector_iterator<typename std::vector<copyable_column>::const_iterator, const matrix_column<ValueType>> const_iterator;
	typedef std::reverse_iterator<iterator> reverse_iterator;
	typedef std::reverse_iterator<const_iterator> const_reverse_iterator;
	iterator begin() noexcept
	{
		return m_column_data.begin();
	}
	const_iterator begin() const noexcept
	{
		return m_column_data.begin();
	}
	const_iterator cbegin() const noexcept
	{
		return m_column_data.cbegin();
	}
	iterator end() noexcept
	{
		return m_column_data.end();
	}
	const_iterator end() const noexcept
	{
		return m_column_data.end();
	}
	const_iterator cend() const noexcept
	{
		return m_column_data.cend();
	}
	reverse_iterator rbegin() noexcept
	{
		return m_column_data.rbegin();
	}
	const_reverse_iterator rbegin() const noexcept
	{
		return m_column_data.rbegin();
	}
	const_reverse_iterator crbegin() const noexcept
	{
		return m_column_data.crbegin();
	}
	reverse_iterator rend() noexcept
	{
		return m_column_data.rend();
	}
	const_reverse_iterator rend() const noexcept
	{
		return m_column_data.rend();
	}
	const_reverse_iterator crend() const noexcept
	{
		return m_column_data.crend();
	}
	ValueType determinant() const
	{
		std::size_t n = m_columns;
		if (!n || n != m_rows)
			throw invalid_matrix_dimensions();
		std::size_t count = n * n;
		unsigned permute_sign = 0u;
		ValueType result{ValueType(1)};
		std::vector<ValueType> ref;
		ref.reserve(count);
		if (m_elements[0] != ValueType())
			ref = m_elements;
		else
		{
			std::size_t i;
			for (i = 1u; i < n; ++i)
			{
				if (m_elements[i * n] != ValueType())
				{
					permute_sign |= i & n - i;
					std::copy(m_elements.begin() + i * n, m_elements.begin() + count, std::back_inserter(ref));
					std::copy_n(m_elements.begin(), i * n, std::back_inserter(ref));
					break;
				}
			}
			if (i == n)
				return ValueType();
		}
		for (std::size_t i = 0; i < n - 1; ++i)
		{
			ValueType* col_ptr = ref.data() + n * i;
			if (col_ptr[i] == ValueType())
			{
				std::size_t j;
				for (j = i + 1; j < n; ++j)
				{
					ValueType* other_col_ptr = ref.data() + n * j;
					if (other_col_ptr[i] != ValueType())
					{
						for (std::size_t k = i; k < n; ++k)
							std::swap(col_ptr[k], other_col_ptr[k]);
						++permute_sign;
						break;
					}
				}
				if (j == n)
					return ValueType();
			}
			ValueType recip = multiplicative_inverse(col_ptr[i]);
			result *= col_ptr[i];
			for (std::size_t j = i + 1; j < n; ++j)
			{
				ValueType* adjusted_col_ptr = ref.data() + n * j;
				if (adjusted_col_ptr[i] == ValueType())
					continue;
				ValueType scaling = recip * adjusted_col_ptr[i];
				adjusted_col_ptr[i] = ValueType();
				for (std::size_t k = i + 1; k < n; ++k)
					adjusted_col_ptr[k] -= scaling * col_ptr[k];
			}
		}
		result *= ref[n * n - 1];
		return (permute_sign & 1) == 1?-result:result;
	}
	const matrix& operator+() const &
	{
		return *this;
	}
	matrix&& operator+() &&
	{
		return static_cast<matrix&&>(*this);
	}
	matrix& operator+=(const matrix& right)
	{
		if (m_columns != right.m_columns || m_rows != right.m_rows)
			throw invalid_matrix_dimensions();
		for (std::size_t i = 0; i < m_elements.size(); ++i)
			m_elements[i] += right.m_elements[i];
		return *this;
	}
	matrix operator+(const matrix& right) const
	{
		if (m_columns != right.m_columns || m_rows != right.m_rows)
			throw invalid_matrix_dimensions();
		matrix result;
		result.m_columns = m_columns;
		result.m_rows = m_rows;
		result.m_elements.reserve(m_elements.size());
		for (std::size_t i = 0; i < m_elements.size(); ++i)
			result.m_elements.emplace_back(m_elements[i] + right.m_elements[i]);
		result.m_column_data.insert(result.m_column_data.end(), m_columns, copyable_column(&result));
		return result;
	}
	matrix operator-() const
	{
		matrix result;
		result.m_columns = m_columns;
		result.m_rows = m_rows;
		result.m_elements.reserve(m_elements.size());
		for (std::size_t i = 0; i < m_elements.size(); ++i)
			result.m_elements.emplace_back(-m_elements[i]);
		result.m_column_data.insert(result.m_column_data.end(), m_columns, copyable_column(&result));
		return result;
	}
	matrix& operator-=(const matrix& right)
	{
		if (m_columns != right.m_columns || m_rows != right.m_rows)
			throw invalid_matrix_dimensions();
		for (std::size_t i = 0; i < m_elements.size(); ++i)
			m_elements[i] -= right.m_elements[i];
		return *this;
	}
	matrix operator-(const matrix& right) const
	{
		if (m_columns != right.m_columns || m_rows != right.m_rows)
			throw invalid_matrix_dimensions();
		matrix result;
		result.m_columns = m_columns;
		result.m_rows = m_rows;
		result.m_elements.reserve(m_elements.size());
		for (std::size_t i = 0; i < m_elements.size(); ++i)
			result.m_elements.emplace_back(m_elements[i] - right.m_elements[i]);
		result.m_column_data.insert(result.m_column_data.end(), m_columns, copyable_column(&result));
		return result;
	}
	bool operator==(const matrix& right) const noexcept
	{
		if (m_columns != right.m_columns || m_rows != right.m_rows)
			return false;
		for (std::size_t i = 0; i < m_elements.size(); ++i)
			if (m_elements[i] != right.m_elements[i])
				return false;
		return true;
	}
	matrix operator*(const matrix& right) const
	{
		if (m_columns != right.m_rows)
			throw invalid_matrix_dimensions();
		matrix result;
		result.m_columns = right.m_columns;
		result.m_rows = m_rows;
		result.m_elements.reserve(m_rows * right.m_columns);

		for (std::size_t c = 0; c < right.m_columns; ++c)
		{
			auto col = result.m_elements.end();
			for (std::size_t r = 0; r < m_rows; ++r)
				result.m_elements.emplace_back(m_elements[r] * right.m_elements[c * m_columns]);
			for (std::size_t i = 1; i < m_columns; ++i)
				for (std::size_t r = 0; r < m_rows; ++r)
					result.m_elements[c * m_rows + r] += m_elements[i * m_rows + r] * right.m_elements[c * m_columns + i];
		}
		result.m_column_data.insert(result.m_column_data.end(), result.m_columns, copyable_column(&result));
		return result;
	}
	matrix& operator*=(const matrix& right)
	{
		return *this = *this * right;
	}
	matrix operator/(const matrix& right) const
	{
		return *this * right.reciprocal();
	}
	matrix& operator/=(const matrix& right)
	{
		return *this *= right.reciprocal();
	}
	matrix reciprocal() const
	{
		if (m_columns != m_rows)
			throw invalid_matrix_dimensions();
		std::vector<std::size_t> perm;
		matrix result;
		std::size_t count = m_columns * m_columns;
		perm.reserve(m_columns);
		result.m_columns = result.m_rows = m_columns;
		result.m_elements.reserve(count);
		std::size_t start_column;
		const matrix& src = *this;
		for (start_column = 0; start_column < m_columns; ++start_column)
			if (src[start_column][0] != ValueType())
				break;
		if (start_column == m_columns)
			throw singular_matrix_inversion();
		ValueType recip = multiplicative_inverse(src[start_column][0]);
		std::copy(src[start_column].begin(), src[start_column].end(), std::back_inserter(result.m_elements));
		std::size_t c_start = start_column + 1, c_end = m_columns;
		for (int N_ = 0; N_ < 2; ++N_)
		{
			for (std::size_t c = c_start; c < c_end; ++c)
			{
				ValueType mult = recip * src[c][0];
				result.m_elements.emplace_back(mult);
				for (std::size_t r = 1; r < m_rows; ++r)
					result.m_elements.emplace_back(src[c][r] - src[start_column][r] * mult);
			}
			c_start = 0u;
			c_end = start_column;
		}
		result.m_column_data.insert(result.m_column_data.end(), m_columns, copyable_column(&result));

		for (std::size_t perm_val = m_columns - start_column; perm_val < m_columns; ++perm_val)
			perm.emplace_back(perm_val);
		for (std::size_t perm_val = 0; perm_val < m_columns - start_column; ++perm_val)
			perm.emplace_back(perm_val);
		for (std::size_t c = 1; c < m_columns; ++c)
		{
			if (result[c][c] == ValueType())
			{
				std::size_t c_swap;
				for (c_swap = c + 1; c_swap < m_columns; ++c_swap)
					if (result[c_swap][c] != ValueType())
						break;
				if (c_swap == m_columns)
					throw singular_matrix_inversion();
				for (std::size_t r = 0; r < m_rows; ++r)
					std::swap(result[c][r], result[c_swap][r]);
				std::swap(perm[perm[perm[c]]], perm[perm[perm[c_swap]]]);
			}
			recip = multiplicative_inverse(result[c][c]);
			for (std::size_t c_reduced = c + 1; c_reduced < m_columns; ++c_reduced)
			{
				if (result[c_reduced][c] == ValueType())
					continue;
				auto mult = recip * result[c_reduced][c];
				result[c_reduced][c] = mult;
				for (std::size_t r = c + 1; r < m_rows; ++r)
					result[c_reduced][r] -= result[c][r] * mult;
			}
		}
		for (std::size_t r = 0; r < m_rows; ++r)
		{
			ValueType mult = multiplicative_inverse(result[r][r]);
			for (std::size_t c = 0; c < r; ++c)
			{
				auto other_product = ValueType();
				for (std::size_t i = c; i < r; ++i)
					other_product -= result[i][r] * result[c][i];
				result[c][r] = mult * other_product;
			}
			result[r][r] = mult;
		}
		for (std::size_t c_inv = 1; c_inv < m_columns; ++c_inv)
		{
			auto c = m_columns - c_inv;
			for (std::size_t r_inv = 1; r_inv <= c; ++r_inv)
			{
				auto r = c - r_inv;
				for (std::size_t i = r + 1; i < c; ++i)
					result[c][r] += result[i][r] * result[c][i];
				result[c][r] = -result[c][r];
			}
		}
		for (std::size_t c = 0; c < m_columns; ++c)
		{
			for (std::size_t r = 0; r < c; ++r)
			{
				auto acc = ValueType();
				for (std::size_t i = c; i < m_columns; ++i)
					acc += result[i][r] * result[c][i];
				result[c][r] = acc;
			}
			for (std::size_t r = c; r < m_columns; ++r)
			{
				auto acc = ValueType();
				for (std::size_t i = r + 1; i < m_columns; ++i)
					acc += result[i][r] * result[c][i];
				result[c][r] += acc;
			}
		}
		for (std::size_t c = 0; c < m_columns; ++c)
			for (std::size_t r = 0; r < m_columns; ++r)
				if (r < perm[r])
					std::swap(result[c][r], result[c][perm[r]]);
		return result;
	}
	matrix transpose() const
	{
		auto result = matrix(m_rows, m_columns);
		for (std::size_t r = 0; r < m_rows; ++r)
			for (std::size_t c = 0; c < m_columns; ++c)
				result[r][c] = this->at(c)[r];
		return result;
	}
};

template <class ValueType>
std::size_t matrix_column<ValueType>::rows() const noexcept
{
	return m_host_matrix->m_rows;
}
template <class ValueType>
const ValueType* matrix_column<ValueType>::data() const noexcept
{
	return m_host_matrix->m_elements.data() + (this - static_cast<const matrix_column<ValueType>*>(m_host_matrix->m_column_data.data())) * m_host_matrix->m_rows;
}
template <class ValueType>
ValueType* matrix_column<ValueType>::data() noexcept
{
	return m_host_matrix->m_elements.data() + (this - static_cast<matrix_column<ValueType>*>(m_host_matrix->m_column_data.data())) * m_host_matrix->m_rows;
}
template <class ValueType>
std::size_t matrix_column<ValueType>::size() const noexcept
{
	return m_host_matrix->m_rows;
}
template <class ValueType, class BaseIterator, class BaseIteratorValueType>
typename matrix<ValueType>::template column_vector_iterator<BaseIterator, BaseIteratorValueType> operator+(std::ptrdiff_t n,
	const typename matrix<ValueType>::template column_vector_iterator<BaseIterator, BaseIteratorValueType>& it)
{
	return it + n;
}

template <class T, std::convertible_to<T> ScalarType>
matrix<T> operator*(const matrix<T>& m, const ScalarType& val)
{
	matrix<T> result(m.columns(), m.rows());
	for (std::size_t c = 0; c < m.columns(); ++c)
		for (std::size_t r = 0; r < m.rows(); ++r)
			result[c][r] = m[c][r] * val;
	return result;
}
template <class T, std::convertible_to<T> ScalarType>
matrix<T> operator*(const ScalarType& val, const matrix<T>& m)
{
	matrix<T> result(m.columns(), m.rows());
	for (std::size_t c = 0; c < m.columns(); ++c)
		for (std::size_t r = 0; r < m.rows(); ++r)
			result[c][r] = val * m[c][r];
	return result;
}
template <class T, std::convertible_to<T> ScalarType>
matrix<T>& operator*=(matrix<T>& m, const ScalarType& val)
{
	for (std::size_t c = 0; c < m.columns(); ++c)
		for (std::size_t r = 0; r < m.rows(); ++r)
			m[c][r] *= val;
	return m;
}
template <class T, std::convertible_to<T> ScalarType>
matrix<T> operator/(const matrix<T>& m, const ScalarType& val)
{
	matrix<T> result(m.columns(), m.rows());
	for (std::size_t c = 0; c < m.columns(); ++c)
		for (std::size_t r = 0; r < m.rows(); ++r)
			result[c][r] = m[c][r] / val;
	return result;
}
template <class T, std::convertible_to<T> ScalarType>
matrix<T> operator/(const ScalarType& val, const matrix<T>& m)
{
	matrix<T> result(m.columns(), m.rows());
	for (std::size_t c = 0; c < m.columns(); ++c)
		for (std::size_t r = 0; r < m.rows(); ++r)
			result[c][r] = val * m[c][r].reciprocal();
	return result;
}
template <class T, std::convertible_to<T> ScalarType>
matrix<T>& operator/=(matrix<T>& m, const ScalarType& val)
{
	for (std::size_t c = 0; c < m.columns(); ++c)
		for (std::size_t r = 0; r < m.rows(); ++r)
			m[c][r] /= val;
	return m;
}

template <class ValueType, std::size_t column_count, std::size_t row_count>
class fixed_matrix :public matrix<ValueType>
{
	using base = matrix<ValueType>;
public:
	typedef ValueType value_type, *pointer, &reference;
	typedef const ValueType *const_pointer, &const_reference;
	typedef std::size_t size_type;
	typedef std::ptrdiff_t difference_type;
	typedef typename base::iterator iterator;
	typedef typename base::const_iterator const_iterator;
	typedef typename base::reverse_iterator reverse_iterator;
	typedef typename base::const_reverse_iterator const_reverse_iterator;

	fixed_matrix():base(column_count, row_count) {}
	explicit fixed_matrix(const ValueType& diagonal_element):base(diagonal_element, column_count, row_count) {}
	fixed_matrix(std::initializer_list<std::initializer_list<ValueType>> elements):base(elements) {
		if (this->rows() != row_count || this->columns() != column_count)
			throw invalid_matrix_dimensions();

	}
	explicit fixed_matrix(const ValueType* data):base(data, column_count, row_count) {}
	template <class T = ValueType, class = std::enable_if_t<std::is_convertible_v<int, T>>>
	explicit fixed_matrix(int v):base(v, column_count, row_count) {}
	fixed_matrix(const std::vector<ValueType>& v):base(v, column_count, row_count) {}
	fixed_matrix(std::vector<ValueType>&& v):base(std::move(v), column_count, row_count) {}
	template <std::input_iterator IteratorBegin, std::input_iterator IteratorEnd>
	fixed_matrix(IteratorBegin first, IteratorEnd last)
		requires std::constructible_from<ValueType, typename std::iterator_traits<IteratorBegin>::reference>
			&& std::same_as<std::remove_cv_t<typename std::iterator_traits<IteratorBegin>::value_type>, std::remove_cv_t<typename std::iterator_traits<IteratorEnd>::value_type>>
		:base(first, last, column_count, row_count) {
	}
	template <std::convertible_to<ValueType> OtherValueType>
	fixed_matrix(const std::vector<OtherValueType>& v):base(v) {}

	const fixed_matrix& operator+() const & {return static_cast<const fixed_matrix&>(+static_cast<const base&>(*this));}
	fixed_matrix&& operator+() && {return static_cast<fixed_matrix&&>(+static_cast<base&&>(*this));}
	fixed_matrix& operator+=(const fixed_matrix& right) {return static_cast<fixed_matrix&>(static_cast<base&>(*this) += right);}
	fixed_matrix operator+(const fixed_matrix& right) const {
		fixed_matrix result;
		static_cast<base&>(result) = static_cast<const base&>(*this) + right;
		return result;
	}
	fixed_matrix operator-() const {
		fixed_matrix result;
		static_cast<base&>(result) = -static_cast<const base&>(*this);
		return result;
	}
	fixed_matrix& operator-=(const fixed_matrix& right) {return static_cast<fixed_matrix&>(static_cast<base&>(*this) -= right);}
	fixed_matrix operator-(const fixed_matrix& right) const {
		fixed_matrix result;
		static_cast<base&>(result) = static_cast<const base&>(*this) - right;
		return result;
	}
	fixed_matrix& operator*=(const fixed_matrix<ValueType, column_count, column_count>& right) {return static_cast<fixed_matrix&>(static_cast<base&>(*this) *= right);}
	template<std::size_t multiplicand_column_count>
	auto operator*(const fixed_matrix<ValueType, multiplicand_column_count, column_count>& right) const {
		fixed_matrix<ValueType, multiplicand_column_count, row_count> result;
		static_cast<base&>(result) = static_cast<const base&>(*this) * right;
		return result;
	}
	fixed_matrix& operator/=(const fixed_matrix<ValueType, column_count, column_count>& right) {return static_cast<fixed_matrix&>(static_cast<base&>(*this) /= right);}
	fixed_matrix operator/(const fixed_matrix<ValueType, column_count, column_count>& right) const {
		fixed_matrix result;
		static_cast<base&>(result) = static_cast<const base&>(*this) / right;
		return result;
	}
	template <std::size_t C = column_count, std::size_t R = row_count>
	auto reciprocal() const ->  std::enable_if_t<C == R, fixed_matrix>
	{
		fixed_matrix result;
		static_cast<base&>(result) = this->base::reciprocal();
		return result;
	}
	fixed_matrix transpose() const {
		fixed_matrix<ValueType, row_count, column_count> result;
		static_cast<base&>(result) = this->base::transpose();
		return result;
	}
};

template <class T, std::size_t column_count, std::size_t row_count, std::convertible_to<T> ScalarType>
fixed_matrix<T, column_count, row_count> operator*(const fixed_matrix<T, column_count, row_count>& m, const ScalarType& val) {
	fixed_matrix<T, column_count, row_count> result;
	static_cast<matrix<T>&>(result) = static_cast<const matrix<T>&>(m) * val;
	return result;
}

template <class T, std::size_t column_count, std::size_t row_count, std::convertible_to<T> ScalarType>
fixed_matrix<T, column_count, row_count> operator*(const ScalarType& val, const fixed_matrix<T, column_count, row_count>& m) {
	fixed_matrix<T, column_count, row_count> result;
	static_cast<matrix<T>&>(result) = val * static_cast<const matrix<T>&>(m);
	return result;
}

template <class T, std::size_t column_count, std::size_t row_count, std::convertible_to<T> ScalarType>
fixed_matrix<T, column_count, row_count>& operator*=(fixed_matrix<T, column_count, row_count>& m, const ScalarType& val) {
	return static_cast<fixed_matrix<T, column_count, row_count>&>(static_cast<matrix<T>&>(m) *= val);
}

template <class T, std::size_t column_count, std::size_t row_count, std::convertible_to<T> ScalarType>
fixed_matrix<T, column_count, row_count> operator/(const fixed_matrix<T, column_count, row_count>& m, const ScalarType& val) {
	fixed_matrix<T, column_count, row_count> result;
	static_cast<matrix<T>&>(result) = static_cast<const matrix<T>&>(m) / val;
	return result;
}

template <class T, std::size_t column_count, std::size_t row_count, std::convertible_to<T> ScalarType>
fixed_matrix<T, column_count, row_count> operator/(const ScalarType& val, const fixed_matrix<T, column_count, row_count>& m) {
	fixed_matrix<T, column_count, row_count> result;
	static_cast<matrix<T>&>(result) = val / static_cast<const matrix<T>&>(m);
	return result;
}

template <class T, std::size_t column_count, std::size_t row_count, std::convertible_to<T> ScalarType>
fixed_matrix<T, column_count, row_count>& operator/=(fixed_matrix<T, column_count, row_count>& m, const ScalarType& val) {
	return static_cast<fixed_matrix<T, column_count, row_count>&>(static_cast<matrix<T>&>(m) /= val);
}

#include <iostream>
#include <iomanip>

template <class ValueType>
std::ostream& operator<<(std::ostream& os, const matrix<ValueType>& m)
{
	if (m.empty())
		return os << "<empty matrix>";
	for (std::size_t r = 0u; r < m.rows(); ++r)
	{
		os << '[' << m[0u][r];
		for (std::size_t c = 1u; c < m.columns(); ++c)
			os << '\t' << m[c][r];
		os << ']';
		if (r < m.rows())
			os << '\n';
	}
	return os;
}

template <class ValueType>
auto show_matrix(const matrix<ValueType>& matr, std::streamsize ls = 0)
{
	for (std::size_t r = 0; r < matr.rows(); ++r)
	{
		std::cout << std::setw(ls + 1) << '[' << matr[0][r];
		for (std::size_t c = 1; c < matr.columns(); ++c)
			std::cout << ", " << matr[c][r];
		std::cout << "]\n";
	}
};
