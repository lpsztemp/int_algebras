#include <iomanip>
#include <iostream>
#include <string_view>
#include <algorithm>
#include <ranges>

#include "as_binary.h"
#include "groups.h"
#include "matrix.h"
#include "int_ring.h"
#include "galois_field.h"
#include "is_prime.h"

#include <string>
#include <sstream>

bool test_gcd()
{
	if (gcd(12, 16, 20) != 4)
		return false;
	if (gcd(32, 16, -20) != 4)
		return false;
	if (gcd(0, 1, 2, 3) != 1)
		return false;
	if (gcd(3, 2, 1, 0) != 1)
		return false;
	if (gcd(0, 0, 0, 0, 0) != 0)
		return false;
	if (lcm(12, -16, 20) != 240)
		return false;
	if (lcm(32, 16, 20) != 160)
		return false;
	if (lcm(0, 1, 2, 3) != 0)
		return false;
	if (lcm(3, 2, 1, 0) != 0)
		return false;
	if (lcm(0, 0, 0, 0, 0) != 0)
		return false;
	if (lcm(0x80000000u, 0x40000000u, 0x20000000u, 2) != 0x80000000u)
		return false;
	return true;
}

bool test_as_binary()
{
	auto test = [](auto val, std::string_view expected)
	{
		std::ostringstream os;
		os << as_binary(val);
		return os.str() == expected;
	};
	if (!test(0, "0"))
		return false;
	if (!test(4, "100"))
		return false;
	if (!test(6, "110"))
		return false;
	if (!test(0x7fffffffffffff20lu, "111111111111111111111111111111111111111111111111111111100100000"))
		return false;
	if (!test(0xfffffffffffffffelu, 
		"1111111111111111111111111111111111111111111111111111111111111110"))
		return false;
	
	return true;
}

bool test_binscan()
{
	if (bsf(0) != 32)
		return false;
	if (bsr(0) != 32)
		return false;
	if (bsf(4) != 2)
		return false;
	if (bsr(4) != 2)
		return false;
	if (bsf(6) != 1)
		return false;
	if (bsr(6) != 2)
		return false;
	if (bsf(256) != 8)
		return false;
	if (bsr(256) != 8)
		return false;
	if (bsf(0x7fffffffffffff20lu) != 5)
		return false;
	if (bsr(0x7fffffffffffff20lu) != 62)
		return false;
	for (unsigned i = 0; i < std::numeric_limits<unsigned long long>::digits; ++i)
	{
		auto val = 1ull << i;
		if (bsf(val) != i)
			return false;
		if (bsr(val) != i)
			return false;
		if ((bsr(--val) & (std::numeric_limits<unsigned long long>::digits - 1)) != i - !!i)
			return false;
		if (bsf(~val) != i)
			return false;
	}
	
	return true;
}

bool test_qr_inverse()
{
	int prime = 1009;
	for (auto i = 1; i < prime; ++i)
		if (auto result = i * qr_inverse(i, prime) % prime; result != 1)
			return false;
	if (qr_inverse(9u, 10u) != 9)
		return false;
	auto uprime = static_cast<unsigned>(prime);
	for (auto i = 1u; i < uprime; ++i)
		if (auto result = i * qr_inverse(i, uprime) % uprime; result != 1)
			return false;
	return true;
}

bool test_qring_pow()
{
	if (pow(QuotientRingElement<unsigned, 37u>(12), 123) != QuotientRingElement<unsigned, 37>(10))
		return false;
	if ((pow(make_qring_element<37>(12), 123) <=> make_qring_element<37>(10)) != 0)
		return false; //same as above
	if (pow(make_qring_element<256>(12), 123) != make_qring_element<256>(0))
		return false;
	if (pow(make_qring_element<37>(12), -123) != make_qring_element<37>(26))
		return false;
	if (pow(make_qring_element<104729>(99999), -2147483648) != make_qring_element<104729>(16810))
		return false;
	if (pow(make_qring_element<0>(999), 999) != make_qring_element<0>(933064983))
		return false;
	if (pow(make_qring_element<2083>(12), -1000000000) != make_qring_element<2083>(1088))
		return false;
	if (pow(make_qring_element<0>(241), -1000000000) != make_qring_element<0>(1309122561))
		return false;
	if (pow(make_qring_element<256>(12), 123u) != make_qring_element<256>(0))
		return false;
	if (pow(make_qring_element<0>(999), 999u) != make_qring_element<0>(933064983))
		return false;
	if (pow(make_qring_element<256>(5), (1llu << 32)) != make_qring_element<256>(1))
		return false;
	if (pow(make_qring_element<0>(5), (1llu << 32)) != make_qring_element<0>(1))
		return false;
	if (pow(make_qring_element<256>(5), (1ll << 32)) != make_qring_element<256>(1))
		return false;
	if (pow(make_qring_element<0>(5), (1ll << 32)) != make_qring_element<0>(1))
		return false;
	return true;
}

template <class RingType>
bool test_qring_sqrt()
{
	using qring_underlying_type = typename RingType::value_type;
	constexpr auto max_elem = qring_underlying_type(-RingType(1));
	auto abs = [max_elem](RingType elem) {return elem <= RingType{static_cast<qring_underlying_type>(max_elem >> 1)}?elem:-RingType(elem);};
	for (qring_underlying_type i = 0; i <= max_elem; ++i)
	{
		auto val = RingType{i};
		auto val_squared = pow(val, 2);
		auto val_squared_rooted = sqrt(val_squared);
		if (abs(val) != val_squared_rooted)
			return false;
	}
	return true;
}

bool test_qring()
{
	using Z10 = QuotientRingElement<unsigned, 10>;
	if (Z10{3} > Z10{5})
		return false;
	if (Z10{3} >= Z10{5})
		return false;
	if (Z10{5} < Z10{3})
		return false;
	if (Z10{5} <= Z10{3})
		return false;
	if (Z10{5} == Z10{3})
		return false;
	if (Z10{5} != Z10{5})
		return false;
	
	if (3 > Z10{5})
		return false;
	if (3 >= Z10{5})
		return false;
	if (5 < Z10{3})
		return false;
	if (5 <= Z10{3})
		return false;
	if (5 == Z10{3})
		return false;
	if (5 != Z10{5})
		return false;
	
	if (Z10{3} > 5)
		return false;
	if (Z10{3} >= 5)
		return false;
	if (Z10{5} < 3)
		return false;
	if (Z10{5} <= 3)
		return false;
	if (Z10{5} == 3)
		return false;
	if (Z10{5} != 5)
		return false;

	if (-make_qring_element<104711>(50000) != make_qring_element<104711>(54711))
		return false;
	if (-QuotientRingElement<unsigned char>{100} != QuotientRingElement<unsigned char>{156})
		return false;
	if (-QuotientRingElement<unsigned long long>{0x100000000ull} != QuotientRingElement<unsigned long long>{0xFFFFFFFF00000000ull})
		return false;

	if (make_qring_element<104711>(50000).reciprocal() * make_qring_element<104711>(50000) != make_qring_element<104711>(1))
		return false;
	if (make_qring_element<104711>(104513) * make_qring_element<104711>(104513).reciprocal() != make_qring_element<104711>(1))
		return false;
	if (QuotientRingElement<unsigned char>{163} * QuotientRingElement<unsigned char>{163}.reciprocal() != QuotientRingElement<unsigned char>{1})
		return false;
	if (QuotientRingElement<unsigned long long>{18446744073709551557ull} * QuotientRingElement<unsigned long long>{18446744073709551557ull}.reciprocal() != QuotientRingElement<unsigned long long>{1})
		return false;
	for (auto i = make_qring_element<1009>(1); i < make_qring_element<1009>(1008); ++i)
		if (i * i.reciprocal() != 1 || i.reciprocal() * i != 1)
			return false;
	if (make_qring_element<7>(5) / make_qring_element<7>(4) != 3)
		return false;

	if (!test_qring_pow())
		return false;
	if (!test_qring_sqrt<QuotientRingElement<unsigned char, 43u>>())
		return false;
	if (!test_qring_sqrt<QuotientRingElement<unsigned, 151u>>())
		return false;
	return true;
}

bool test_galois_pow()
{
	if (pow(galois_element<unsigned, value_based_irreducable<unsigned, 0x201B>>{120}, 12300) != 5290u)
		return false;
	if (pow(galois_element<unsigned, value_based_irreducable<unsigned, 0x201B>>{120}, -123456789) != 818)
		return false;
	if (pow(galois_element<unsigned>{0x12345678}, 12300) != 3209385094u)
		return false;
	if (pow(galois_element<unsigned>{0x12345678}, -2147483648) != 1470156439)
		return false;
	if (pow(galois_element<unsigned, value_based_irreducable<unsigned, 0x201B>>{120}, 456789012) != 7683)
		return false;
	if (pow(galois_element<unsigned>{0x12345678}, -0x789ABCDE) != 1502989952)
		return false;
	if (pow(galois_element<unsigned>{0x12345678}, 12300u) != 3209385094u)
		return false;
	if (pow(galois_element<unsigned, value_based_irreducable<unsigned, 0x201B>>{120}, 456789012u) != 7683)
		return false;

	return true;
}

bool test_galois()
{
	if (3_ge > 5_ge)
		return false;
	if (3_ge >= 5_ge)
		return false;
	if (5_ge < 3_ge)
		return false;
	if (5_ge <= 3_ge)
		return false;
	if (5_ge == 3_ge)
		return false;
	if (5_ge != 5_ge)
		return false;
	
	if (3 > 5_ge)
		return false;
	if (3 >= 5_ge)
		return false;
	if (5 < 3_ge)
		return false;
	if (5 <= 3_ge)
		return false;
	if (5 == 3_ge)
		return false;
	if (5 != 5_ge)
		return false;
	
	if (3_ge > 5)
		return false;
	if (3_ge >= 5)
		return false;
	if (5_ge < 3)
		return false;
	if (5_ge <= 3)
		return false;
	if (5_ge == 3)
		return false;
	if (5_ge != 5)
		return false;

	if (3_ge + 5_ge != 6_ge)
		return false;
	if ((123456_ge).reciprocal() * 123456_ge != 1_ge)
		return false;
	if (123456_ge * (123456_ge).reciprocal() != 1_ge)
		return false;
	if (galois_element<unsigned>{UINT_MAX}.reciprocal() * galois_element<unsigned>{UINT_MAX} != 1)
		return false;
	if (galois_element<unsigned>{UINT_MAX} * galois_element<unsigned>{UINT_MAX}.reciprocal() != 1)
		return false;
	if (galois_element<unsigned long long>{123456}.reciprocal() * galois_element<unsigned long long>{123456} != 1)
		return false;
	if (galois_element<unsigned long long>{123456} * galois_element<unsigned long long>{123456}.reciprocal() != 1)
		return false;
	if (galois_element<unsigned long long>{ULLONG_MAX}.reciprocal() * galois_element<unsigned long long>{ULLONG_MAX} != 1)
		return false;
	if (galois_element<unsigned long long>{ULLONG_MAX} * galois_element<unsigned long long>{ULLONG_MAX}.reciprocal() != 1)
		return false;
	{
		using gf = galois_element<unsigned, value_based_irreducable<unsigned, 0x80000009u>>;
		if (gf{0x7FFFFFFFu}.reciprocal() * gf{0x7FFFFFFFu} != 1u)
			return false;
		if (gf{0x7FFFFFFFu} * gf{0x7FFFFFFFu}.reciprocal() != 1u)
			return false;
	}
	{
		using gf = galois_element<unsigned char>;
		for (unsigned i = 1; i < UCHAR_MAX; ++i)
		{
			auto val = gf{static_cast<unsigned char>(i)};
			if (val * val.reciprocal() != 1u || val.reciprocal() * val != 1u)
				return false;
		}
	}
	if (galois_element<unsigned char>{5} / galois_element<unsigned char>{4} != 202)
		return false;
	{
		using gf = galois_element<unsigned char, value_based_irreducable<unsigned, 0b100011011>>;
		if (gf{5} / gf{4} != 202)
			return false;
	}
	{
		using gf = galois_element<unsigned, value_based_irreducable<unsigned, 0B100011011>>;
		for (unsigned i = 0; i < 1u << gf::polynomial::degree; ++i)
		{
			if (pow(sqrt(gf(i)), 2) != i)
				return false;
		}
	}
	if (!test_galois_pow())
		return false;
	return true;
}

bool test_matrix()
{
	using Z37 = QuotientRingElement<unsigned, 37>;
	matrix<Z37> K1 =
	{
		{ Z37{3},  Z37{4}, Z37{23}},
		{Z37{10}, Z37{17}, Z37{31}},
		{Z37{35}, Z37{33}, Z37{19}}
	};
	matrix<Z37> K2 =
	{
		{Z37{14}},
		{Z37{31}},
		{Z37{32}}
	};
	matrix<Z37> P =
	{
		{Z37{13}},
		{Z37{9}},
		{Z37{17}}
	};
	if (K1.determinant() != 10)
		return false;
	auto K1_2 = K1 + K1;
	if (K1_2 != matrix<Z37>
	{
		{Z37{6},      Z37{8},       Z37{9}},
		{Z37{20},     Z37{34},      Z37{25}},
		{Z37{33},     Z37{29},      Z37{1}}
	})
		return false;
	if ((K1_2 += K1_2) != matrix<Z37>
	{
		{Z37{12},     Z37{16},      Z37{18}},
		{ Z37{3},     Z37{31},      Z37{13}},
		{Z37{29},     Z37{21},       Z37{2}}
	})
		return false;
	if ((K1_2 -= K1) != matrix<Z37>
	{
		{ Z37{9},     Z37{12},      Z37{32}},
		{Z37{30},     Z37{14},      Z37{19}},
		{Z37{31},     Z37{25},      Z37{20}}
	})
		return false;
	if ((K1_2 *= K1_2) != matrix<Z37>
	{
		{Z37{27},      Z37{3},       Z37{9}},
		{Z37{21},     Z37{32},      Z37{15}},
		{Z37{21},      Z37{1},      Z37{17}}
	})
		return false;
	if ((K1_2 /= K1) != matrix<Z37>
	{
		{Z37{27},     Z37{36},      Z37{22}},
		{Z37{16},      Z37{5},      Z37{20}},
		{Z37{19},      Z37{1},      Z37{23}}
	})
		return false;
	if (K1 * K2 != matrix<Z37>
	{
		{Z37{14}},
		{Z37{31}},
		{Z37{12}}
	})
		return false;
	if (K1.reciprocal() != matrix<Z37>
	{
		{ Z37{4},     Z37{35},      Z37{14}},
		{Z37{34},     Z37{14},      Z37{10}},
		{Z37{29},     Z37{30},      Z37{27}}
	})
		return false;
	auto C = K1 * P + K2;
	if (C != matrix<Z37>
	{
		{Z37{36}},
		{Z37{27}},
		{Z37{34}}
	})
		return false;
	if (K1.reciprocal() * (C - K2) != matrix<Z37>
	{
		{Z37{13}},
		{Z37{9}},
		{Z37{17}}
	})
		return false;
	{
		matrix<int> exp =
		{
			{1, 0, 0, 0},
			{0, 1, 0, 0},
			{0, 0, 1, 0},
			{0, 0, 0, 1}
		};
		matrix<int> test = matrix<int>(1, 4, 4);
		if (!std::equal(test.data(), test.data() + test.size(), exp.data()))
			return false;
	}
	{
		matrix<int> exp =
		{
			{1, 0, 0, 0},
			{0, 1, 0, 0}
		};
		matrix<int> test = matrix<int>(1, 4, 2);
		if (!std::equal(test.data(), test.data() + test.size(), exp.data()))
			return false;
	}
	{
		matrix<int> exp =
		{
			{1, 0},
			{0, 1},
			{0, 0},
			{0, 0}
		};
		matrix<int> test = matrix<int>(1, 2, 4);
		if (!std::equal(test.data(), test.data() + test.size(), exp.data()))
			return false;
	}
	return true;
}

bool test_fixed_matrix()
{
	using Z37 = QuotientRingElement<unsigned, 37>;
	fixed_matrix<Z37, 3, 3> K1 =
	{
		{ Z37{3},  Z37{4}, Z37{23}},
		{Z37{10}, Z37{17}, Z37{31}},
		{Z37{35}, Z37{33}, Z37{19}}
	};
	fixed_matrix<Z37, 1, 3> K2 =
	{
		{Z37{14}},
		{Z37{31}},
		{Z37{32}}
	};
	fixed_matrix<Z37, 1, 3> P =
	{
		{Z37{13}},
		{Z37{9}},
		{Z37{17}}
	};
	if (K1.determinant() != 10)
		return false;
	auto K1_2 = K1 + K1;
	if (K1_2 != fixed_matrix<Z37, 3, 3>
	{
		{Z37{6},      Z37{8},       Z37{9}},
		{Z37{20},     Z37{34},      Z37{25}},
		{Z37{33},     Z37{29},      Z37{1}}
	})
		return false;
	if ((K1_2 += K1_2) != fixed_matrix<Z37, 3, 3>
	{
		{Z37{12},     Z37{16},      Z37{18}},
		{ Z37{3},     Z37{31},      Z37{13}},
		{Z37{29},     Z37{21},       Z37{2}}
	})
		return false;
	if ((K1_2 -= K1) != fixed_matrix<Z37, 3, 3>
	{
		{ Z37{9},     Z37{12},      Z37{32}},
		{Z37{30},     Z37{14},      Z37{19}},
		{Z37{31},     Z37{25},      Z37{20}}
	})
		return false;
	if ((K1_2 *= K1_2) != fixed_matrix<Z37, 3, 3>
	{
		{Z37{27},      Z37{3},       Z37{9}},
		{Z37{21},     Z37{32},      Z37{15}},
		{Z37{21},      Z37{1},      Z37{17}}
	})
		return false;
	if ((K1_2 /= K1) != fixed_matrix<Z37, 3, 3>
	{
		{Z37{27},     Z37{36},      Z37{22}},
		{Z37{16},      Z37{5},      Z37{20}},
		{Z37{19},      Z37{1},      Z37{23}}
	})
		return false;
	if (K1 * K2 != fixed_matrix<Z37, 1, 3>
	{
		{Z37{14}},
		{Z37{31}},
		{Z37{12}}
	})
		return false;
	if (K1.reciprocal() != fixed_matrix<Z37, 3, 3>
	{
		{ Z37{4},     Z37{35},      Z37{14}},
		{Z37{34},     Z37{14},      Z37{10}},
		{Z37{29},     Z37{30},      Z37{27}}
	})
		return false;
	auto C = K1 * P + K2;
	if (C != fixed_matrix<Z37, 1, 3>
	{
		{Z37{36}},
		{Z37{27}},
		{Z37{34}}
	})
		return false;
	if (K1.reciprocal() * (C - K2) != fixed_matrix<Z37, 1, 3>
	{
		{Z37{13}},
		{Z37{9}},
		{Z37{17}}
	})
		return false;
	{
		fixed_matrix<int, 4, 4> exp =
		{
			{1, 0, 0, 0},
			{0, 1, 0, 0},
			{0, 0, 1, 0},
			{0, 0, 0, 1}
		};
		fixed_matrix<int, 4, 4> test = fixed_matrix<int, 4, 4>(1);
		if (!std::equal(test.data(), test.data() + test.size(), exp.data()))
			return false;
	}
	{
		fixed_matrix<int, 4, 2> exp =
		{
			{1, 0, 0, 0},
			{0, 1, 0, 0}
		};
		fixed_matrix<int, 4, 2> test = fixed_matrix<int, 4, 2>(1);
		if (!std::equal(test.data(), test.data() + test.size(), exp.data()))
			return false;
	}
	{
		fixed_matrix<int, 2, 4> exp =
		{
			{1, 0},
			{0, 1},
			{0, 0},
			{0, 0}
		};
		fixed_matrix<int, 2, 4> test = fixed_matrix<int, 2, 4>(1);
		if (!std::equal(test.data(), test.data() + test.size(), exp.data()))
			return false;
	}
	return true;
}

bool test_groups()
{
	{
		std::size_t i = 0;
		for (auto v:Group{additive_integral_set<unsigned, 20u>{}, [](auto x, auto y) {return (x + y) % 20u;}, [](auto val) {return 20u - val;}})
			if (i++ != v)
				return false;
	}
	{
		const unsigned control[] = {1, 3, 7, 9, 11, 13, 17, 19};
		auto it = std::rbegin(control);
		auto g = Group{multiplicative_integral_set<unsigned, 20>{}, [](auto x, auto y) {return (x * y) % 20u;}, [](auto x) {return qr_inverse(x, 20u);}};
		for (auto v : g | std::views::reverse)
			if (*it++ != v)
				return false;
	}
	{
		using ring = QuotientRingElement<unsigned, 16>;
		auto set = std::ranges::iota_view{0u, 16u} | std::views::transform([](auto val) {return ring(val);});
		auto g = Group{set, std::plus<>{}, std::negate<>{}};
		if (g.order() != 16)
			return false;
		const unsigned ctrl_0[] = {0};
		auto sg = Group{cyclic_set<ring, std::plus<>, std::negate<>>(g[0])};
		if (!std::ranges::equal(ctrl_0, sg, {}, [](auto n) {return ring{n};}))
			return false;
		sg = Group{cyclic_set<ring, std::plus<>, std::negate<>>(g[1])};
		const unsigned ctrl_1[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
		if(!std::ranges::equal(ctrl_1, sg, {}, [](auto n) {return ring{n};}))
			return false;
		sg = Group{cyclic_set<ring, std::plus<>, std::negate<>>(g[2])};
		const unsigned ctrl_2[] = {0, 2, 4, 6, 8, 10, 12, 14};
		if(!std::ranges::equal(ctrl_2, sg, {}, [](auto n) {return ring{n};}))
			return false;
		sg = Group{cyclic_set<ring, std::plus<>, std::negate<>>(g[3])};
		const unsigned ctrl_3[] = {0, 3, 6, 9, 12, 15, 2, 5, 8, 11, 14, 1, 4, 7, 10, 13};
		if(!std::ranges::equal(ctrl_3, sg, {}, [](auto n) {return ring{n};}))
			return false;
		sg = Group{cyclic_set<ring, std::plus<>, std::negate<>>(g[4])};
		const unsigned ctrl_4[] = {0, 4, 8, 12};
		if(!std::ranges::equal(ctrl_4, sg, {}, [](auto n) {return ring{n};}))
			return false;
		sg = Group{cyclic_set<ring, std::plus<>, std::negate<>>(g[5])};
		const unsigned ctrl_5[] = {0, 5, 10, 15, 4, 9, 14, 3, 8, 13, 2, 7, 12, 1, 6, 11};
		if(!std::ranges::equal(ctrl_5, sg, {}, [](auto n) {return ring{n};}))
			return false;
		sg = Group{cyclic_set<ring, std::plus<>, std::negate<>>(g[6])};
		const unsigned ctrl_6[] = {0, 6, 12, 2, 8, 14, 4, 10};
		if(!std::ranges::equal(ctrl_6, sg, {}, [](auto n) {return ring{n};}))
			return false;
		sg = Group{cyclic_set<ring, std::plus<>, std::negate<>>(g[7])};
		const unsigned ctrl_7[] = {0, 7, 14, 5, 12, 3, 10, 1, 8, 15, 6, 13, 4, 11, 2, 9};
		if(!std::ranges::equal(ctrl_7, sg, {}, [](auto n) {return ring{n};}))
			return false;
		sg = Group{cyclic_set<ring, std::plus<>, std::negate<>>(g[8])};
		const unsigned ctrl_8[] = {0, 8};
		if(!std::ranges::equal(ctrl_8, sg, {}, [](auto n) {return ring{n};}))
			return false;
		sg = Group{cyclic_set<ring, std::plus<>, std::negate<>>(g[9])};
		const unsigned ctrl_9[] = {0, 9, 2, 11, 4, 13, 6, 15, 8, 1, 10, 3, 12, 5, 14, 7};
		if(!std::ranges::equal(ctrl_9, sg, {}, [](auto n) {return ring{n};}))
			return false;
		sg = Group{cyclic_set<ring, std::plus<>, std::negate<>>(g[10])};
		const unsigned ctrl_10[] = {0, 10, 4, 14, 8, 2, 12, 6};
		if(!std::ranges::equal(ctrl_10, sg, {}, [](auto n) {return ring{n};}))
			return false;
		sg = Group{cyclic_set<ring, std::plus<>, std::negate<>>(g[11])};
		const unsigned ctrl_11[] = {0, 11, 6, 1, 12, 7, 2, 13, 8, 3, 14, 9, 4, 15, 10, 5};
		if(!std::ranges::equal(ctrl_11, sg, {}, [](auto n) {return ring{n};}))
			return false;
		sg = Group{cyclic_set<ring, std::plus<>, std::negate<>>(g[12])};
		const unsigned ctrl_12[] = {0, 12, 8, 4};
		if(!std::ranges::equal(ctrl_12, sg, {}, [](auto n) {return ring{n};}))
			return false;
		sg = Group{cyclic_set<ring, std::plus<>, std::negate<>>(g[13])};
		const unsigned ctrl_13[] = {0, 13, 10, 7, 4, 1, 14, 11, 8, 5, 2, 15, 12, 9, 6, 3};
		if(!std::ranges::equal(ctrl_13, sg, {}, [](auto n) {return ring{n};}))
			return false;
		sg = Group{cyclic_set<ring, std::plus<>, std::negate<>>(g[14])};
		const unsigned ctrl_14[] = {0, 14, 12, 10, 8, 6, 4, 2};
		if(!std::ranges::equal(ctrl_14, sg, {}, [](auto n) {return ring{n};}))
			return false;
		sg = Group{cyclic_set<ring, std::plus<>, std::negate<>>(g[15])};
		const unsigned ctrl_15[] = {0, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
		if(!std::ranges::equal(ctrl_15, sg, {}, [](auto n) {return ring{n};}))
			return false;
	}
	{
		auto g = Group{multiplicative_integral_set<unsigned, 16>{}, [](auto x, auto y) {return (x * y) % 16;}, [](auto x) {return qr_inverse(x, 16u);}};
		if (g.order() != 8)
			return false;
		const unsigned ctrl_0[] = {1};
		auto sg = g.cyclic_subgroup(g[0]);
		if (!std::ranges::equal(ctrl_0, sg, {}, [](auto n) {return unsigned{n};}))
			return false;
		sg = g.cyclic_subgroup(g[1]);
		const unsigned ctrl_1[] = {1, 3, 9, 11};
		if(!std::ranges::equal(ctrl_1, sg, {}, [](auto n) {return unsigned{n};}))
			return false;
		sg = g.cyclic_subgroup(g[2]);
		const unsigned ctrl_2[] = {1, 5, 9, 13};
		if(!std::ranges::equal(ctrl_2, sg, {}, [](auto n) {return unsigned{n};}))
			return false;
		sg = g.cyclic_subgroup(g[3]);
		const unsigned ctrl_3[] = {1, 7};
		if(!std::ranges::equal(ctrl_3, sg, {}, [](auto n) {return unsigned{n};}))
			return false;
		sg = g.cyclic_subgroup(g[4]);
		const unsigned ctrl_4[] = {1, 9};
		if(!std::ranges::equal(ctrl_4, sg, {}, [](auto n) {return unsigned{n};}))
			return false;
		sg = g.cyclic_subgroup(g[5]);
		const unsigned ctrl_5[] = {1, 11, 9, 3};
		if(!std::ranges::equal(ctrl_5, sg, {}, [](auto n) {return unsigned{n};}))
			return false;
		sg = g.cyclic_subgroup(g[6]);
		const unsigned ctrl_6[] = {1, 13, 9, 5};
		if(!std::ranges::equal(ctrl_6, sg, {}, [](auto n) {return unsigned{n};}))
			return false;
		sg = g.cyclic_subgroup(g[7]);
		const unsigned ctrl_7[] = {1, 15};
		if(!std::ranges::equal(ctrl_7, sg, {}, [](auto n) {return unsigned{n};}))
			return false;
	}
	{
		using gf = galois_element<unsigned, value_based_irreducable<unsigned, 0x13>>;
		auto g = Group{std::views::iota(0u, 16u) | std::views::transform([](auto val){return gf{val};}), std::plus<>{}, std::negate<>{}};
		auto sg = g.cyclic_subgroup(g[0]);
		if (sg.order() != 1 || sg[0] != 0)
			return false;
		for (auto gen:g | std::views::drop(1))
		{
			auto sg = g.cyclic_subgroup(gen);
			if (sg.order() != 2)
				return false;
			if (sg[0] != gf{} || sg[1] != gen)
				return false;
		}
	}
	{
		using gf = galois_element<unsigned, value_based_irreducable<unsigned, 0x13>>;
		auto g = Group{std::views::iota(1u, 16u) | std::views::transform([](auto val){return gf{val};}), std::multiplies<>{}, [](auto val) {return val.reciprocal();}};
		if (g.order() != 15u)
			return false;
		auto sg = g.cyclic_subgroup(g[0]);
		const unsigned ctrl_0[] = {1};
		if (!std::ranges::equal(ctrl_0, sg, {}, [](auto n) {return gf{n};}))
			return false;
		sg = g.cyclic_subgroup(g[1]);
		const unsigned ctrl_1[] = {1, 2, 4, 8, 3, 6, 12, 11, 5, 10, 7, 14, 15, 13, 9};
		if(!std::ranges::equal(ctrl_1, sg, {}, [](auto n) {return gf{n};}))
			return false;
		sg = g.cyclic_subgroup(g[2]);
		const unsigned ctrl_2[] = {1, 3, 5, 15, 2, 6, 10, 13, 4, 12, 7, 9, 8, 11, 14};
		if(!std::ranges::equal(ctrl_2, sg, {}, [](auto n) {return gf{n};}))
			return false;
		sg = g.cyclic_subgroup(g[3]);
		const unsigned ctrl_3[] = {1, 4, 3, 12, 5, 7, 15, 9, 2, 8, 6, 11, 10, 14, 13};
		if(!std::ranges::equal(ctrl_3, sg, {}, [](auto n) {return gf{n};}))
			return false;
		sg = g.cyclic_subgroup(g[4]);
		const unsigned ctrl_4[] = {1, 5, 2, 10, 4, 7, 8, 14, 3, 15, 6, 13, 12, 9, 11};
		if(!std::ranges::equal(ctrl_4, sg, {}, [](auto n) {return gf{n};}))
			return false;
		sg = g.cyclic_subgroup(g[5]);
		const unsigned ctrl_5[] = {1, 6, 7};
		if(!std::ranges::equal(ctrl_5, sg, {}, [](auto n) {return gf{n};}))
			return false;
		sg = g.cyclic_subgroup(g[6]);
		const unsigned ctrl_6[] = {1, 7, 6};
		if(!std::ranges::equal(ctrl_6, sg, {}, [](auto n) {return gf{n};}))
			return false;
		sg = g.cyclic_subgroup(g[7]);
		const unsigned ctrl_7[] = {1, 8, 12, 10, 15};
		if(!std::ranges::equal(ctrl_7, sg, {}, [](auto n) {return gf{n};}))
			return false;
		sg = g.cyclic_subgroup(g[8]);
		const unsigned ctrl_8[] = {1, 9, 13, 15, 14, 7, 10, 5, 11, 12, 6, 3, 8, 4, 2};
		if(!std::ranges::equal(ctrl_8, sg, {}, [](auto n) {return gf{n};}))
			return false;
		sg = g.cyclic_subgroup(g[9]);
		const unsigned ctrl_9[] = {1, 10, 8, 15, 12};
		if(!std::ranges::equal(ctrl_9, sg, {}, [](auto n) {return gf{n};}))
			return false;
		sg = g.cyclic_subgroup(g[10]);
		const unsigned ctrl_10[] = {1, 11, 9, 12, 13, 6, 15, 3, 14, 8, 7, 4, 10, 2, 5};
		if(!std::ranges::equal(ctrl_10, sg, {}, [](auto n) {return gf{n};}))
			return false;
		sg = g.cyclic_subgroup(g[11]);
		const unsigned ctrl_11[] = {1, 12, 15, 8, 10};
		if(!std::ranges::equal(ctrl_11, sg, {}, [](auto n) {return gf{n};}))
			return false;
		sg = g.cyclic_subgroup(g[12]);
		const unsigned ctrl_12[] = {1, 13, 14, 10, 11, 6, 8, 2, 9, 15, 7, 5, 12, 3, 4};
		if(!std::ranges::equal(ctrl_12, sg, {}, [](auto n) {return gf{n};}))
			return false;
		sg = g.cyclic_subgroup(g[13]);
		const unsigned ctrl_13[] = {1, 14, 11, 8, 9, 7, 12, 4, 13, 10, 6, 2, 15, 5, 3};
		if(!std::ranges::equal(ctrl_13, sg, {}, [](auto n) {return gf{n};}))
			return false;
		sg = g.cyclic_subgroup(g[14]);
		const unsigned ctrl_14[] = {1, 15, 10, 12, 8};
		if(!std::ranges::equal(ctrl_14, sg, {}, [](auto n) {return gf{n};}))
			return false;
	}

	return true;
}

bool test_is_prime()
{
	if (is_small_prime(1))
		return false;
	if (!is_small_prime(2))
		return false;
	if (!is_small_prime(3))
		return false;
	if (is_small_prime(4))
		return false;
	if (is_small_prime(123))
		return false;
	if (is_small_prime(520))
		return false;
	if (!is_small_prime(521))
		return false;
	if (is_small_prime(522))
		return false;
	if (!is_small_prime(523))
		return false;
	if (is_small_prime(524))
		return false;
	if (is_small_prime(527))
		return false;
	if (is_small_prime(990))
		return false;
	if (!is_small_prime(991))
		return false;
	if (is_small_prime(992))
		return false;
	if (is_small_prime(993))
		return false;
	if (is_small_prime(994))
		return false;
	if (is_small_prime(995))
		return false;
	if (is_small_prime(996))
		return false;
	if (!is_small_prime(997))
		return false;
	if (is_small_prime(998))
		return false;
	if (is_small_prime(993982))
		return false;
	if (!is_small_prime(993983))
		return false;
	if (is_small_prime(993984))
		return false;
	if (!is_small_prime(993997))
		return false;
	if (is_small_prime(994001))
		return false;
	if (is_small_prime(994009))
		return false;
	if (is_small_prime(994010))
		return false;
	if (!is_small_prime(0x100060007ll))
		return false;
	if (is_small_prime(0x100060009ull))
		return false;
	return true;
}

bool test_eulers_totient()
{
	if (eulers_totient(2u) != 1)
		return false;
	if (eulers_totient(35) != 24)
		return false;
	if (eulers_totient(64) != 32)
		return false;
	if (eulers_totient(72) != 24)
		return false;
	if (eulers_totient(0xF619F619u) != 0xF51E0000)
		return false;
	if (eulers_totient(0x100060007ll) != 0x100060006ll)
		return false;
	if (eulers_totient(281281747415761ull) != 281277454414320ull)
		return false;
	return true;
}

bool test_carmichael()
{
	if (carmichael(1) != 1)
		return false;
	if (carmichael(2) != 1)
		return false;
	if (carmichael(4) != 2)
		return false;
	if (carmichael(5) != 4)
		return false;
	if (carmichael(6) != 2)
		return false;
	if (carmichael(7) != 6)
		return false;
	if (carmichael(8) != 2)
		return false;
	if (carmichael(11) != 10)
		return false;
	if (carmichael(12) != 2)
		return false;
	if (carmichael(27) != 18)
		return false;
	if (carmichael(32) != 8)
		return false;
	if (carmichael(1936) != 220)
		return false;
	if (carmichael(4096) != 1024)
		return false;
	if (carmichael(9998) != 4998)
		return false;
	if (carmichael(0x100000000ull) != 1u << 30)
		return false;
	if (carmichael(0x8001800000000000ull) != 0x1000200000000000ull)
		return false;
	return true;
}

bool test_pow()
{
	if (pow(2, 2, 3) != 1)
		return false;
	if (pow(2, -1, 3) != 2)
		return false;
	if (pow(12, 123, 37) != 10)
		return false;
	if (pow(12, 123, 256) != 0)
		return false;
	if (pow(12, -123, 37) != 26)
		return false;
	if (pow(99999, -2147483648, 104729) != 16810)
		return false;
	if (pow(999, 999, 0x100000000ll) != 933064983)
		return false;
	if (pow(12, -1000000000, 2083) != 1088)
		return false;
	if (pow(241, -1000000000, 0x100000000ll) != 1309122561)
		return false;
	if (pow(12, 123u, 256) != 0)
		return false;
	if (pow(5, (1llu << 32), 256) != 1)
		return false;
	if (pow(5u, (1llu << 32), 0x100000000ll) != 1)
		return false;
	if (pow(0, 0, 0) != 1)
		return false;
	if (pow(0, 0, 1) != 0)
		return false;
	if (pow(0, 13, 0) != 0)
		return false;
	if (pow(3, 3, 0) != 27)
		return false;
	if (pow(3, 3, 1) != 0)
		return false;
	if (pow(3, 3, 25) != 2)
		return false;
	if (pow(3, 3, -25) != 2)
		return false;
	if (pow(-3, 3, 25) != 23)
		return false;
	if (pow(-3, 3, -25) != 23)
		return false;
	if (pow(3, -3, 0) != 0x684bda13)
		return false;
	if (pow(3, -3, 1) != 0)
		return false;
	if (pow(3, -3, 25) != 13)
		return false;
	if (pow(3, -3, -25) != 13)
		return false;
	if (pow(-3, -3, 25) != 12)
		return false;
	if (pow(-3, -3, -25) != 12)
		return false;
	return true;
}

bool test_primitive_root()
{
	if (!is_primitive_kth_root(2, 3, 7))
		return false;
	if (is_primitive_kth_root(2, 4, 3))
		return false;
	if (!is_primitive_kth_root(9, 4, 32))
		return false;
	if (is_primitive_kth_root(9, 8, 32))
		return false;

	if (!is_primitive_kth_root(get_primitive_kth_root(3u, 7u), 3u, 7u))
		return false;
	if (!is_primitive_kth_root(get_primitive_kth_root(4ull, 0x100000000ull), 4ull, 0x100000000ull))
		return false;
	if (get_primitive_kth_root(4u, 0u) != get_primitive_kth_root(4ull, 0x100000000ull))
		return false;
	if (!is_primitive_kth_root(2ull, 48ull, 281474976710655ull))
		return false;

	if (!is_primitive_kth_root(get_primitive_kth_root(4u, 0u), 4u, 0u))
		return false;
	if (!is_primitive_kth_root(get_primitive_kth_root(8u, 0u), 8u, 0u))
		return false;
	if (!is_primitive_kth_root(get_primitive_kth_root(16u, 0u), 16u, 0u))
		return false;

	if (!is_primitive_kth_root(get_primitive_kth_root(2ull, 0ull), 2ull, 0ull))
		return false;
	if (!is_primitive_kth_root(get_primitive_kth_root(4ull, 0ull), 4ull, 0ull))
		return false;
	if (!is_primitive_kth_root(get_primitive_kth_root(8ull, 0ull), 8ull, 0ull))
		return false;

	return true;
}

#define run(x) if (!x) do { \
		std::cerr << "Failure of " << #x << " at line " << __LINE__ << ".\n"; \
		std::exit(__LINE__); \
	} while (0)

int main(int, char**)
{
	run(test_as_binary());
	run(test_binscan());
	run(test_gcd());
	run(test_is_prime());
	run(test_eulers_totient());
	run(test_pow());
	run(test_carmichael());
	run(test_primitive_root());
	run(test_qr_inverse());
	run(test_qring());
	run(test_galois());
	run(test_matrix());
	run(test_fixed_matrix());
	run(test_groups());

	return 0;
}

