#include <type_traits>
#include <iostream>
#include <limits>
#include <concepts>

#ifndef ABS_ALG_AS_BINARY_H_
#define ABS_ALG_AS_BINARY_H_

template <std::unsigned_integral T>
class as_binary
{
	constexpr static auto bit_count = std::numeric_limits<T>::digits;
	template <std::size_t i = 0, T ... vals>
	static constexpr auto init_array() noexcept
	{
		if constexpr (i == bit_count + 1)
			return std::array<char, bit_count + 1>{vals...};
		else
			return init_array<i + 1, 0, vals...>();
	}
	std::array<char, bit_count + 1> m_data = init_array();
	const char* m_ptr = m_data.data();
public:
	constexpr explicit as_binary(T value) noexcept
	{
		auto count = bit_count;
		auto v = T(value);
		m_data[count] = 0;
		if (!v)
			m_data[--count] = '0';
		else do
		{
			m_data[--count] = (v & 1) + '0';
		}while ((v >>= 1) != 0);
		m_ptr = m_data.data() + count;
	}
	constexpr const char* c_str() const noexcept
	{
		return m_ptr;
	}
};

template <class T> as_binary(T val) -> as_binary<std::make_unsigned_t<T>>;

template <class TraitsType, class Elem>
decltype(auto) operator<<(std::basic_ostream<char, TraitsType>& os, const as_binary<Elem>& val)
{
	return os << val.c_str();
}

#endif //ABS_ALG_AS_BINARY_H_
