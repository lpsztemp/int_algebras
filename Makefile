ifeq ($(CXX), )
CXX=g++
endif

MYINCLUDES=$(wildcard *.h)

.phony: all clean

all:linux/Debug/tests.out linux/Release/tests.out linux/Debug/demo.out linux/Release/demo.out

linux/Release/tests.out:linux/Release/obj/tests.obj
	$(CXX) linux/Release/obj/tests.obj -o linux/Release/tests.out

linux/Release/demo.out:linux/Release/obj/demo.obj
	$(CXX) linux/Release/obj/demo.obj -o linux/Release/demo.out

linux/Debug/tests.out:linux/Debug/obj/tests.obj
	$(CXX) linux/Debug/obj/tests.obj -o linux/Debug/tests.out
	
linux/Debug/demo.out:linux/Debug/obj/demo.obj
	$(CXX) linux/Debug/obj/demo.obj -o linux/Debug/demo.out

linux/Release/obj/tests.obj:linux/Release/obj tests.cpp $(MYINCLUDES)
	$(CXX) --std=c++20 -m64 -O3 -c tests.cpp -o linux/Release/obj/tests.obj
	
linux/Release/obj/demo.obj:linux/Release/obj demo.cpp $(MYINCLUDES)
	$(CXX) --std=c++20 -m64 -O3 -c demo.cpp -o linux/Release/obj/demo.obj

linux/Debug/obj/tests.obj:linux/Debug/obj tests.cpp $(MYINCLUDES)
	$(CXX) --std=c++20 -m64 -g -c tests.cpp -o linux/Debug/obj/tests.obj
	
linux/Debug/obj/demo.obj:linux/Debug/obj demo.cpp $(MYINCLUDES)
	$(CXX) --std=c++20 -m64 -g -c demo.cpp -o linux/Debug/obj/demo.obj

linux/Release/obj:
	mkdir -p linux/Release/obj

linux/Debug/obj:
	mkdir -p linux/Debug/obj

clean:
	rm -rf linux
