#pragma once
#include <stdexcept>
#include <concepts>
#include <compare>
#include <type_traits>
#include <limits>
#include "util.h"
#include "gcd.h"
#include "bit_scan.h"
#include "is_prime.h"

#ifdef _MSC_VER
#pragma warning (disable: 4146) //unary minus applied to an unsigned type
#endif //_MSC_VER

template <std::unsigned_integral UnderlyingType, UnderlyingType count = UnderlyingType()>
class QuotientRingElement
{
	UnderlyingType m_val = UnderlyingType();
public:
	using value_type = UnderlyingType;
	using size_type = std::size_t;
public:
	static constexpr UnderlyingType modulo = count;
	QuotientRingElement() = default;
	constexpr explicit QuotientRingElement(UnderlyingType init) noexcept(count == UnderlyingType()) :m_val(init)
	{
		if constexpr (count != UnderlyingType())
		{
			if (init >= count)
				throw std::domain_error("Not a ring element");
		}
	}
	QuotientRingElement& operator++() noexcept
	{
		if (++m_val == count)
			m_val = value_type();
		return *this;
	}
	QuotientRingElement operator++(int) noexcept
	{
		auto old = *this;
		++*this;
		return old;
	}
	QuotientRingElement& operator--() noexcept
	{
		if (m_val-- == UnderlyingType())
			m_val = count - 1;
		return *this;
	}
	QuotientRingElement operator--(int) noexcept
	{
		auto old = *this;
		--*this;
		return old;
	}
	constexpr QuotientRingElement operator+(const QuotientRingElement& right) const noexcept
	{
		return QuotientRingElement{add_mod(m_val, right.m_val, count)};
	}
	QuotientRingElement& operator+=(const QuotientRingElement& right) noexcept
	{
		return *this = *this + right;
	}
	constexpr QuotientRingElement operator-() const noexcept
	{
		return m_val == UnderlyingType()?QuotientRingElement():QuotientRingElement{static_cast<UnderlyingType>(count - m_val)};
	}
	constexpr QuotientRingElement operator-(const QuotientRingElement& right) const noexcept
	{
		return QuotientRingElement{add_mod(m_val, static_cast<UnderlyingType>(count - right.m_val), count)};
	}
	QuotientRingElement& operator-=(const QuotientRingElement& right) noexcept
	{
		return *this = *this - right;
	}
	constexpr QuotientRingElement operator*(const QuotientRingElement& right) const noexcept
	{
		return QuotientRingElement{mul_mod(m_val, right.m_val, count)};
	}
	QuotientRingElement& operator*=(const QuotientRingElement& right) noexcept
	{
		return *this = *this * right;
	}
	constexpr QuotientRingElement reciprocal() const
	{
		if constexpr (count == UnderlyingType())
			return QuotientRingElement{
				static_cast<UnderlyingType>(
					qr_inverse_euclid(subtraction{}, euclidean_division{},
						m_val,
						static_cast<UnderlyingType>(static_cast<UnderlyingType>(-m_val) % m_val),
						UnderlyingType(1),
						static_cast<UnderlyingType>(-(static_cast<UnderlyingType>(-m_val) / m_val + 1))
					)
				)
			};
		else
			return QuotientRingElement{
				static_cast<UnderlyingType>(
					qr_inverse_euclid(subtraction_modulo{count}, euclidean_division{}, count, m_val, UnderlyingType(), UnderlyingType(1))
					)
			};
	}
	constexpr QuotientRingElement operator/(const QuotientRingElement& right) const noexcept
	{
		return *this * right.reciprocal();
	}
	QuotientRingElement& operator/=(const QuotientRingElement& right)
	{
		return *this *= right.reciprocal();
	}
	constexpr explicit operator UnderlyingType() const noexcept
	{
		return m_val;
	}
};

template <std::unsigned_integral T1, T1 count_1, std::unsigned_integral T2, T2 count_2>
requires (static_cast<std::common_type_t<T1, T2>>(count_1) == static_cast<std::common_type_t<T1, T2>>(count_2))
constexpr auto operator<=>(const QuotientRingElement<T1, count_1>& left, const QuotientRingElement<T2, count_2>& right) noexcept
{
	return static_cast<std::common_type_t<T1, T2>>(T1(left)) <=> static_cast<std::common_type_t<T1, T2>>(T2(right));
}
template <std::unsigned_integral T1, T1 count_1, std::unsigned_integral T2, T2 count_2>
requires (static_cast<std::common_type_t<T1, T2>>(count_1) == static_cast<std::common_type_t<T1, T2>>(count_2))
constexpr auto operator==(const QuotientRingElement<T1, count_1>& left, const QuotientRingElement<T2, count_2>& right) noexcept
{
	return static_cast<std::common_type_t<T1, T2>>(T1(left)) == static_cast<std::common_type_t<T1, T2>>(T2(right));
}
template <std::unsigned_integral T1, T1 count, std::convertible_to<T1> T2>
constexpr auto operator<=>(QuotientRingElement<T1, count> left, T2 right) noexcept
{
	return static_cast<std::common_type_t<T1, T2>>(T1(left)) <=> static_cast<std::common_type_t<T1, T2>>(right);
}
template <std::unsigned_integral T1, T1 count, std::convertible_to<T1> T2>
constexpr auto operator==(QuotientRingElement<T1, count> left, T2 right) noexcept
{
	return static_cast<std::common_type_t<T1, T2>>(T1(left)) == static_cast<std::common_type_t<T1, T2>>(right);
}
template <std::unsigned_integral T2, T2 count, std::convertible_to<T2> T1>
constexpr auto operator<=>(T1 left, QuotientRingElement<T2, count> right) noexcept
{
	return static_cast<std::common_type_t<T1, T2>>(left) <=> static_cast<std::common_type_t<T1, T2>>(T2(right));
}
template <std::unsigned_integral T2, T2 count, std::convertible_to<T2> T1>
constexpr auto operator==(T1 left, QuotientRingElement<T2, count> right) noexcept
{
	return static_cast<std::common_type_t<T1, T2>>(left) == static_cast<std::common_type_t<T1, T2>>(T2(right));
}

template <class UnderlyingType, UnderlyingType count, std::integral Power>
constexpr QuotientRingElement<UnderlyingType, count> pow(QuotientRingElement<UnderlyingType, count> x, Power pw) noexcept
{
	if (x == QuotientRingElement<UnderlyingType, count>{})
		return QuotientRingElement<UnderlyingType, count>{};
	else if (pw < 0)
		return pw == std::numeric_limits<Power>::min()?(pow(x, -(std::numeric_limits<Power>::min() + 1)) * x).reciprocal():pow(x.reciprocal(), -pw);
	else
	{
		QuotientRingElement<UnderlyingType, count> result{1u};
		if constexpr (count == UnderlyingType()) //totient = (1 << (sizeof(UnderlyingType) * CHAR_BIT - 1)), then, if x is odd, x^{totient - 1} == 1
		{
			if constexpr (std::numeric_limits<UnderlyingType>::max() <= static_cast<std::make_unsigned_t<Power>>(std::numeric_limits<Power>::max()))
			{
				if ((UnderlyingType(x) & UnderlyingType(1)) != UnderlyingType())
					pw &= static_cast<Power>(std::numeric_limits<UnderlyingType>::max() >> 1);
			}
		}else if constexpr ((count >> 1) <= static_cast<std::make_unsigned_t<Power>>(std::numeric_limits<Power>::max()))
		{
			if constexpr (bsf(count) == bsr(count))
			{
				if ((UnderlyingType(x) & UnderlyingType(1)) != UnderlyingType())
					pw &= static_cast<Power>((count >> 1) - 1);
			}else if constexpr (is_small_prime(count))
				pw %= static_cast<Power>(count - 1);
		}

		{
			QuotientRingElement<UnderlyingType, count> x_power{x};
			while (pw != Power{})
			{
				if (pw & Power{1})
					result *= x_power;
				pw >>= 1;
				x_power *= x_power;
			}
		}
		return result;
	}
}

template <class UnderlyingType, UnderlyingType count>
constexpr bool is_perfect_square(QuotientRingElement<UnderlyingType, count> x)
{
	return pow(x, (count - 1) / 2) == QuotientRingElement<UnderlyingType, count>(1);
}

//count is assumed prime, zero, or a power of 2
template <class UnderlyingType, UnderlyingType count>
constexpr QuotientRingElement<UnderlyingType, count> sqrt(QuotientRingElement<UnderlyingType, count> x)
{
	using Z = QuotientRingElement<UnderlyingType, count>;
	if (x == Z())
		return Z();
	if (pow(x, UnderlyingType((count - 1) / 2)) != Z(1))
		throw std::domain_error("Square root of a quotient ring element does not exist.");
	auto oddity = UnderlyingType(bsf(count - 1));
	auto pw = UnderlyingType(count >> oddity);
	auto factor = Z(2);
	while (pow(factor, UnderlyingType((count - 1) / 2)) != Z(count - 1))
		++factor;
	factor = pow(factor, pw);
	auto test = pow(x, pw);
	auto result = pow(x, UnderlyingType((pw + 1) / 2));
	while (test != Z(1))
	{
		if (test == Z())
			return Z();
		unsigned i = 0;
		while (pow(test, UnderlyingType(1) << i) != Z(1))
			++i;
		auto multiplier = pow(factor, UnderlyingType(1) << (oddity - i - 1));
		oddity = i;
		factor = multiplier * multiplier;
		test *= factor;
		result *= multiplier;
	}
	if (result >= Z((count >> 1) + 1))
		result = -result;
	return result;
}

template <std::size_t modulo, class T>
constexpr auto make_qring_element(T value) noexcept(std::is_unsigned_v<T>)
{
	if constexpr (std::is_signed_v<T>)
	{
		if (value < T())
			throw std::invalid_argument("Ring value should not be negative");
	}
	return QuotientRingElement<std::make_unsigned_t<T>, modulo>(value);
}

#include <iostream>

template <class UnderlyingType, UnderlyingType count>
std::ostream& operator<<(std::ostream& os, const QuotientRingElement<UnderlyingType, count>& right)
{
	return os << static_cast<UnderlyingType>(right);
}

