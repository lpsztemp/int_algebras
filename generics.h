#pragma once
#include <type_traits>

template <class T>
inline const T additive_identity = T(0);

template <class T>
inline const T multiplicative_identity = T(1);

template <class T, class ... Unused>
auto multiplicative_inverse(const T& val, Unused&&...) -> T
{
	return T(1) / val;
}

template <class T>
auto multiplicative_inverse(const T& val) requires requires (const T& x) {x.reciprocal();}
{
	return val.reciprocal();
}

template <class T>
concept additive_group = requires(T& x, const T& y) {
	additive_identity<T>; -x; x = x + y; x += y;
};

template <class T>
concept multiplicative_group = requires(T& x, const T& y) {
	multiplicative_identity<T>; x = x * y; x *= y; x = multiplicative_inverse(y);
};

template <class T>
concept field_type = additive_group<T> && multiplicative_group<T>;
