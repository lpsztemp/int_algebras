#include <stdexcept>
#include "bit_scan.h"

#ifndef POLY_DIV_H_
#define POLY_DIV_H_

struct poly_invalid_by_zero:std::runtime_error
{
	poly_invalid_by_zero():std::runtime_error("Polynomial division by zero") {}
};

template <class T>
struct poly_div_t
{
	T quot, rem;
};

template <class T>
constexpr poly_div_t<T> poly_div(T dividend, T divisor)
{
	if (divisor == T())
		throw poly_invalid_by_zero();
	auto q = T(), r = dividend;
	auto dividend_deg = bsr(dividend);
	auto divisor_deg = bsr(divisor);
	while (dividend_deg < sizeof(T) * 8 && dividend_deg >= divisor_deg)
	{
		auto off = (dividend_deg - divisor_deg);
		q |= T(1) << off;
		r ^= divisor << off;
		dividend_deg = bsr(r);
	}
	return poly_div_t<T>{q, r};
}

#endif //POLY_DIV_H_
